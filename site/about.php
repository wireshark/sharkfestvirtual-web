<?php include($_SERVER['DOCUMENT_ROOT'] . "/header.php"); ?>

<div class="container grid-container" id="main-container">
        <div class="about-page-image">
        </div>
        <!--<div class="row grid-row">
            <div class="col-xs-12 col-centered index-text">
                <h3>Join us for SharkFest'18 US in Berkeley, California.<br>
                Act now to lock in the lowest registration price.</h3>
                <div class="reg-button-front">
                    <a class="front-button" href="register"><h4>Register Now</h4></a>
                </div>
            </div>
        </div>-->
        <section id="main-content">
            <div class="container col-sm-12 about-content">
                  <div class="col-sm-8 col-xs-12 about-text">
                        <div class="about-container">
                              <h2>What is SharkFest?</h2>
                              <p><a href="https://sharkfest.wireshark.org">SharkFest™</a>, launched in 2008, is a series of annual educational conferences staged in various parts of the globe and focused on sharing knowledge, experience and best practices among the <a href="https://wireshark.org">Wireshark®</a> developer and user communities.</p>
             
                              <p>SharkFest attendees hone their skills in the art of packet analysis by attending lecture and lab-based sessions delivered by the most seasoned experts in the industry. Wireshark core code contributors also gather during the conference days to enrich and evolve the tool to maintain its relevance in ensuring the productivity of modern networks.</p>
                              <p>Learn more about SharkFest in our <a href="assets/faq.pdf">FAQ PDF</a>.</p>
                              <h2>What is Wireshark?</h2>
                              <p>Wireshark is the de facto standard and most widely-deployed open-source network and packet analysis tool consistently downloaded 1M+ times/month by IT Professionals across all network-related disciplines.  <a href="https://riverbed.com">Riverbed</a> is the current host and corporate sponsor of the Wireshark project, Wireshark Foundation and SharkFest. </p>
                              <h2>SharkFest Mission</h2>
                              <p>SharkFest’s aim is to support ongoing Wireshark development, to educate and inspire current and future generations of computer science and IT professionals responsible for managing, troubleshooting, diagnosing and securing legacy and modern networks, and to encourage widespread use of the free analysis tool.  Per Gerald Combs, Wireshark project Founder …“Wireshark is a tool and a community.  My job is to support both."</p>

                              <h2>SharkFest GOALS</h2>
                              <ol>
                                    <li>To educate current and future generations of network engineers, network architects, application engineers, network consultants, and other IT professionals in best practices for troubleshooting, securing, analyzing, and maintaining productive, efficient networking infrastructures through use of the Wireshark free, open source analysis tool.</li>
                                    <li>To share use cases and knowledge among members of the Wireshark user and developer communities in a relaxed, informal milieu.</li>
                                    <li>To remain a self-funded, independent, educational conference hosted by a corporate sponsor.</li>
                              </ol>
                              <h2>Code of Conduct</h2>
                              <p>SharkFest™ is dedicated to providing a harassment-free conference experience for everyone, regardless of gender, gender identity and expression, age, sexual orientation, disability, physical appearance, body size, race, ethnicity, religion (or lack thereof), or technology choices. We do not tolerate harassment of conference participants in any form. Sexual language and imagery is not appropriate for any conference venue, including talks, workshops, parties, Twitter and other online media. Conference participants violating these rules may be sanctioned or expelled from the conference without a refund at the discretion of the conference organizers.</p>
                        </div>
                  </div>
                  <div class="col-sm-4 col-xs-12 column-about">
                    <div class="col-xs-12 background-blue front-column1 about-cell">
                        <img src="img/small-jasper.jpg">
                        <div class="grid-cell">
                            <p><a style="color: white" href="/register">Registration is now open!</a></p>
                        </div>
                        <div class="about-button">
                            <a class="" href="register"><h4>Learn More</h4></a>
                        </div>
                    </div>
                    <!-- <div class="col-xs-12 background-blue front-column1 about-cell">
                        <div class="">
                            <div class="grid-cell">
                                <h2>First-Time Attendee?</h2>
                                <p>Learn more about what makes the SharkFest conference so special.</p>
                            </div>
                            <div class="retro-image">
                                
                            </div>
                        </div>
                        <div class="about-button">
                            <a class="" href="first-time"><h4>More Info</h4></a>
                        </div>
                    </div> -->
                </div>
            </div>
        </section>
        <div class="background-blue-aboutpage">
              <section id="why-attend" class="why-attend why-body">
                    <div class="container col-sm-12">
                        <div class="col-sm-12 col-xs-12 about-text">
                              <div class="about-container">
                                    <h2>Why Attend?</h2>
                                    <div class="col-md-6 col-sm-12 why-reasons">
                                          <div class="round-background">
                                                <h2>1</h2>
                                          </div>
                                          <div class="why-reason-text">
                                                <p><strong> Strengthen diagnostic muscle </strong>by attending a selection of Wireshark-centric sessions ranging from beginner to intermediate to advanced levels. </p>
                                          </div>
                                    </div>
                                    <div class="col-md-6 col-sm-12 why-reasons">
                                          <div class="round-background">
                                                <h2>4</h2>
                                          </div>
                                          <div class="why-reason-text">
                                                <p><strong>World-class Wireshark instruction</strong> by renowned veteran packet analysts.</p>
                                          </div>
                                    </div>
                                    <div class="col-md-6 col-sm-12 why-reasons">
                                          <div class="round-background">
                                                <h2>2</h2>
                                          </div>
                                          <div class="why-reason-text">
                                                <p><strong> Meet the developers</strong>. Gerald Combs, founder of the Wireshark open source project, and reps from the global network of core devs will be online, answering questions, and presenting Wireshark's current and future direction.</p>
                                          </div>
                                    </div>
                                    <div class="col-md-6 col-sm-12 why-reasons">
                                          <div class="round-background">
                                                <h2>5</h2>
                                          </div>
                                          <div class="why-reason-text">
                                                <p><strong>  Test your investigative chops</strong> with fellow attendees at the PacketPalooza group packet competition.</p>
                                          </div>
                                    </div>
                                    <div class="col-md-6 col-sm-12 why-reasons">
                                          <div class="round-background">
                                                <h2>3</h2>
                                          </div>
                                          <div class="why-reason-text">
                                                <p><strong> Discover Wireshark use-enhancing technology</strong> from SharkFest sponsors.</p>
                                          </div>
                                    </div>
                                    <div class="col-md-6 col-sm-12 why-reasons">
                                          <div class="round-background">
                                                <h2>6</h2>
                                          </div>
                                          <div class="why-reason-text">
                                                <p><strong> Build your professional network</strong>.</p>
                                          </div>
                                    </div>
                              </div>
                        </div>
                  </div>
              </section>
            </div>
</div>

</div>


<?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php"); ?>
