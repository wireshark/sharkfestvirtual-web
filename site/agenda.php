<?php include($_SERVER['DOCUMENT_ROOT'] . "/header.php"); ?>
<div class="agenda-background-image">
</div>
<div class="row grid-row reg-row">
	<div class="col-lg-7 col-centered index-text">
	    <div class="">
	        <a class="front-button" href="assets/SharkFest20Agenda.pdf"><h4>Conference Agenda PDF</h4></a>
	        <h4>Please note that SharkFest'20 Virtual pre-conference class and sessions times are Pacific Daylight Time.</h4>
	    </div>
	</div>
</div>
	
<div class="container-fluid agenda-container">
	<div class="row">
		<div class="col-md-10 col-centered">
			<div class="row schedule-lvl1">
				<ul class="nav nav-pills nav-justified schedule">
				    <li class="active"><a data-toggle="pill" href="#tab-first"><h4>Day 01<br><span>10.12.2020</span></h4></a></li>
				    <li><a data-toggle="pill" href="#tab-second"><h4>Day 02<br><span>10.13.2020</span></h4></a></li>
				    <li><a data-toggle="pill" href="#tab-third"><h4>Day 03<br><span>10.14.2020</span></h4></a></li>
				    <li><a data-toggle="pill" href="#tab-fourth"><h4>Day 04<br><span>10.15.2020</span></h4></a></li>
				    <li><a data-toggle="pill" href="#tab-fifth"><h4>Day 05<br><span>10.16.2020</span></h4></a></li>
				 </ul>
			</div>
			<div class="tab-content">
				<div id="tab-first" class="table-responsive container tab-pane fade in active">
					<div class="tab-content lvl2">
						<ul class="nav nav-pills nav-justified schedule">
						    <li class="active"><a data-toggle="pill" href="#lvl1-tab-first"><h4>Pre-Conference Class I: Troubleshooting with Wireshark - Core Skills</h4>
						    	<p>(Zoom)</p></a></li>
						 </ul>
						<div id="lvl1-tab-first" class="table-responsive container tab-pane fade in active">
							<table class="schedule-table table">
								<tbody>
									<!-- <tr>
										<th>8:00am - 9:00am</th>
										<th><h4>Check-in & Badge Pick up </h4>
											<p>(Registration Table, Baise Ballroom Foyer)</p>
										</th>
									</tr>
									<tr>
										<th>8:00am - 9:00am</th>
										<th><h4>Breakfast (Basie C/C1)</h4></th>
									</tr> -->
									<tr>
										<th>8:00am - 5:00pm</th>
										<th><h4>Class in session (with breaks)</h4></th>
										
									</tr>
									<!-- <tr>
										<th>12:00pm - 1:00pm</th>
										<th><h4>LUNCH  (Basie C/C1)</h4></th>
									</tr>
									<tr>
										<th>1:00pm - 5:00pm</th>
										<th><h4>Class in session (with afternoon breaks)	 </h4></th>
										
									</tr> -->
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div id="tab-second" class="tab-pane fade table-responsive container">
					<div class="tab-content lvl2">
						<ul class="nav nav-pills nav-justified schedule">
						    <li class="active"><a data-toggle="pill" href="#lvl2-tab-first"><h4>Pre-Conference Class I: Troubleshooting with Wireshark - Core Skills</h4>
						    	<p>(Zoom)</p></a></li>
						 </ul>
						<div id="lvl2-tab-first" class="table-responsive container tab-pane fade in active">
							<table class="schedule-table table">
								<tbody>
									<!-- <tr>
										<th>8:00am - 9:00am</th>
										<th><h4>Check-in & Badge Pick up </h4>
											<p>(Registration Table, Baise Ballroom Foyer)</p>
										</th>
									</tr>
									<tr>
										<th>8:00am - 9:00am</th>
										<th><h4>Breakfast (Basie C/C1)</h4></th>
									</tr> -->
									<tr>
										<th>8:00am - 5:00pm</th>
										<th><h4>Class in session (with breaks)</h4></th>
										
									</tr>
									<!-- <tr>
										<th>12:00pm - 1:00pm</th>
										<th><h4>LUNCH  (Basie C/C1)</h4></th>
									</tr>
									<tr>
										<th>1:00pm - 5:00pm</th>
										<th><h4>Class in session (with afternoon breaks)	 </h4></th>
										
									</tr> -->
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div id="tab-third" class="tab-pane fade table-responsive container">
					<div class="tab-content lvl2">
						<ul class="nav nav-pills nav-justified schedule">
						    <li class="active"><a data-toggle="pill" href="#lvl22-tab-first"><h4>Pre-Conference Class II: Wireshark Profiles - How to Analyze Trace Files Faster and Easier</h4>
						    	<p>(Zoom 1)</p></a></li>
					    	<li class="active"><a data-toggle="pill" href="#lvl22-tab-second"><h4>Pre-Conference Class III: SSL/TLS Troubleshooting with Wireshark</h4>
						    	<p>(Zoom 2)</p></a></li>
						 </ul>
						 <div id="lvl22-tab-first" class="table-responsive container tab-pane fade in active">
							<table class="schedule-table table">
								<tbody>
									<!-- <tr>
										<th>8:00am - 9:00am</th>
										<th><h4>Breakfast (Basie C/C1)</h4></th>
									</tr> -->
									<tr>
										<th>8:00am - 5:00pm</th>
										<th>
											<h4>Class in session (with breaks)</h4>
											
										</th>

									</tr>
									<!-- <tr>
										<th>12:00pm - 1:00pm</th>
										<th><h4>LUNCH (Basie C/C1) </h4></th>
									</tr>
									<tr>
										<th>1:00pm - 5:00pm</th>
										<th><h4>Class in session (with afternoon breaks)</h4></th>
										
									</tr> -->
								</tbody>
							</table>
						</div>
						<div id="lvl22-tab-second" class="table-responsive container tab-pane fade">
							<table class="schedule-table table">
								<tbody>
									<!-- <tr>
										<th>8:00am - 9:00am</th>
										<th><h4>Breakfast (Basie C/C1)</h4></th>
									</tr> -->
									<tr>
										<th>8:00am - 5:00pm</th>
										<th>
											<h4>Class in session (with breaks)</h4>
											
										</th>

									</tr>
									<!-- <tr>
										<th>12:00pm - 1:00pm</th>
										<th><h4>LUNCH (Basie C/C1) </h4></th>
									</tr>
									<tr>
										<th>1:00pm - 5:00pm</th>
										<th><h4>Class in session (with afternoon breaks)</h4></th>
										
									</tr> -->
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div id="tab-fourth" class="tab-pane fade table-responsive container">
					<div class="tab-content lvl2">
						<ul class="nav nav-pills nav-justified schedule">
						    <li class="active"><a data-toggle="pill" href="#lvl23-tab-first"><h4>Zoom 1</h4></a></li>
						    <li><a data-toggle="pill" href="#lvl23-tab-second"><h4>Zoom 2</h4></a></li>
						 </ul>
						 <div id="lvl23-tab-first" class="table-responsive container tab-pane fade in active">
							<table class="schedule-table table">
								<tbody>
									<tr>
										<th>8:00am - 9:00am</th>
										<th>
											<h4 style="color: #ce8c19">Keynote: Latest Wireshark Developments & Road Map</h4>
											<p>Instructors: <a href="bios/gerald-combs">Gerald Combs</a> & Friends</p>
										</th>
									</tr>
									<tr>
										<th>9:30am - 10:30am</th>
										<th><img src="img/littlefin.png"><img src="img/littlefin.png">
											<h4>01: BACNet and Wireshark for Beginners</h4>
											<p>Instructor: <a href="bios/werner-fischer">Werner Fischer</a></p>
										</th>
									</tr>
									<tr>
										<th>10:30am - 10:45am</th>
										<th><h4>Q & A</h4></th>
									</tr>
									<tr>
										<th>11:00am - 12:00pm</th>
										<th><img src="img/littlefin.png"><img src="img/littlefin.png">	
											<h4>03: IPv6 security assessment tools (aka IPv6 hacking tools)</h4>
											<p>Instructor: <a href="bios/jeff-carrell">Jeff Carrell</a></p>
										</th>
									</tr> 
									<tr>
										<th>12:00pm - 12:15pm</th>
										<th><h4>Q & A</h4></th>
									</tr>
									<tr>
										<th>12:15pm - 12:45pm</th>
										<th><h4>Lunch</h4></th>
									</tr>
									<tr>
										<th>12:45pm - 1:45pm</th>
										<th><img src="img/littlefin.png">
											<h4>05: Kismet and Wireless Security 101</h4>
											<p>Instructor: <a href="bios/mike-kershaw">Mike Kershaw</a></p>
										</th>
									</tr>
									<tr>
										<th>1:45pm - 2:00pm</th>
										<th><h4>Q & A</h4></th>
									</tr>
									<tr>
										<th>2:15pm - 3:15pm</th>
										<th><img src="img/littlefin.png">
											<h4>07: TLS encryption and decryption: What every IT engineer should know about TLS</h4>
											<p>Instructor: <a href="bios/ross-bagurdes">Ross Bagurdes</a></p>
										</th>
									</tr> 
									<tr>
										<th>3:15pm - 3:30pm</th>
										<th><h4>Q & A</h4></th>
									</tr>
									<tr>
										<th>3:45pm - 4:45pm</th>
										<th><img src="img/littlefin.png">
											<h4>09: Troubleshooting Cloud Network Outages</h4>
											<p>Instructor: <a href="bios/chris-hull">Chris Hull</a></p>				
										</th>
									</tr>
									<tr>
										<th>4:45pm - 5:00pm</th>
										<th><h4>Q & A</h4></th>
									</tr>
									<tr>
										<th>5:15pm - 6:15pm</th>
										<th><img src="img/littlefin.png"><img src="img/littlefin.png">
											<h4>11: Automation TIPS & tricks Using Wireshark/tshark in Windows</h4>
											<p>Instructor: <a href="bios/megumi-takeshita">Megumi Takeshita</a></p>
										</th>
									</tr>
									<tr>
										<th>6:15pm - 6:30pm</th>
										<th><h4>Q & A</h4></th>
									</tr>
								</tbody>
							</table>
						</div>
						<div id="lvl23-tab-second" class="table-responsive container tab-pane fade">
							<table class="schedule-table table">
								<tbody>
									<tr>
										<th>9:30am - 10:30am</th>
										<th><img src="img/littlefin.png">
										<img src="img/littlefin.png">
										<img src="img/littlefin.png">
											<h4>02: Going down the retransmission hole</h4>
											<p>Instructor: <a href="bios/sake-blok">Sake Blok</a></p>
										</th>
									</tr>
									<tr>
										<th>10:30am - 10:45am</th>
										<th><h4>Q & A</h4></th>
									</tr>
									<tr>
										<th>11:00am - 12:00pm</th>
										<th><img src="img/littlefin.png">
											<img src="img/littlefin.png">		
											<h4>04: Improving packet capture in the DPDK</h4>
											<p>Instructor: <a href="bios/jasper-bongertz">Stephen Hemminger</a></p>
										</th>
									</tr> 
									<tr>
										<th>12:00pm - 12:15pm</th>
										<th><h4>Q & A</h4></th>
									</tr>
									<tr>
										<th>12:15pm - 12:45pm</th>
										<th><h4>Lunch</h4></th>
									</tr>
									<tr>
										<th>12:45pm - 1:45pm</th>
										<th><img src="img/littlefin.png">
										<img src="img/littlefin.png">
											<h4>06: Packets! Wait... What? A very improvised last-minute Wireshark talk about things you can find in pcap files that are funny, interesting or weird. I don't know. Let's find out together</h4>
											<p>Instructor: <a href="bios/jasper-bongertz">Jasper Bongertz</a></p>
										</th>
									</tr>
									
									<tr>
										<th>1:45pm - 2:00pm</th>
										<th><h4>Q & A</h4></th>
									</tr>
									<tr>
										<th>2:15pm - 3:15pm</th>
										<th><img src="img/littlefin.png">
											<h4>08: Why an Enterprise Visibility Platform is critical for effective Packet Analysis?</h4>
											<p>Instructor: <a href="bios/keval-shah">Keval Shah</a></p>
										</th>
									</tr>
									<tr>
										<th>3:15pm - 3:30pm</th>
										<th><h4>Q & A</h4></th>
									</tr>
									<tr>
										<th>3:45pm - 4:45pm</th>
										<th><img src="img/littlefin.png">
										<img src="img/littlefin.png">
											<h4>10: TCP SACK overview & impact on performance (subject to change)</h4>
											<p>Instructor: <a href="bios/john-pittle">John Pittle</a></p>
										</th>
									</tr> 
									<tr>
										<th>4:45pm - 5:00pm</th>
										<th><h4>Q & A</h4></th>
									</tr>
									<tr>
										<th>5:15pm - 6:15pm</th>
										<th><img src="img/littlefin.png">
											<h4>12: How Long is a Packet? And Does it Really Matter?</h4>
											<p>Instructor: <a href="bios/stephen-donnelly">Dr. Stephen Donnelly</a></p>					
										</th>
									</tr>
									<tr>
										<th>6:15pm - 6:30pm</th>
										<th><h4>Q & A</h4></th>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div id="tab-fifth" class="tab-pane fade table-responsive container">
					<div class="tab-content lvl2">
						<ul class="nav nav-pills nav-justified schedule">
						    <li class="active"><a data-toggle="pill" href="#lvl24-tab-first"><h4>Zoom 1</h4></a></li>
						    <li><a data-toggle="pill" href="#lvl24-tab-second"><h4>Zoom 2</h4></a></li>

						 </ul>
						 <div id="lvl24-tab-first" class="table-responsive container tab-pane fade in active">
					<table class="schedule-table table">
						<tbody>
							<tr>
								<th>8:00am - 9:00am</th>
								<th><h4>Keynote: Vern Paxson, Professor of EECS, UC Berkeley/ Chief Scientist, Corelight, Inc.</h4></th>
							</tr>
							<tr>
								<th>9:30am - 10:30am</th>
								<th><img src="img/littlefin.png">
								<img src="img/littlefin.png">
									<h4>13: Dissector Session TBA</h4>
									<p>Instructor: <a href="bios/roland-knall">Roland Knall</a></p>
								</th>
							</tr>
							<tr>
								<th>10:30am - 10:45am</th>
								<th><h4>Q & A</h4></th>
							</tr>
							<tr>
								<th>11:00am - 12:00pm</th>
								<th><img src="img/littlefin.png">
									<img src="img/littlefin.png">		
									<h4>15: TLS decryption examples</h4>
									<p>Instructor: <a href="bios/peter-wu">Peter Wu</a></p>
								</th>
							</tr> 
							<tr>
								<th>12:00pm - 12:15pm</th>
								<th><h4>Q & A</h4></th>
							</tr>
							<tr>
								<th>12:15pm - 12:45pm</th>
								<th><h4>Lunch</h4></th>
							</tr>
							<tr>
								<th>12:45pm - 1:45pm</th>
								<th><img src="img/littlefin.png">
								<img src="img/littlefin.png">
									<h4>17: Analyzing Honeypot Traffic</h4>
									<p>Instructor: <a href="bios/tom-peterson">Tom Peterson</a></p>
								</th>
							</tr>
							<tr>
								<th>1:45pm - 2:00pm</th>
								<th><h4>Q & A</h4></th>
							</tr>
							<tr>
								<th>2:15pm - 3:15pm</th>
								<th><img src="img/littlefin.png">
								<img src="img/littlefin.png">
									<h4>19: The Other Protocols (used in LTE)</h4>
									<p>Instructor: <a href="bios/mark-stout">Mark Stout</a></p>
								</th>
							</tr> 
							<tr>
								<th>3:15pm - 3:30pm</th>
								<th><h4>Q & A</h4></th>
							</tr>
							<tr>
								<th>3:45pm - 4:45pm</th>
								<th><img src="img/littlefin.png">
								<img src="img/littlefin.png">
									<h4>21: Ostinato - craft packets, generate traffic</h4>
									<p>Instructor: <a href="bios/srivats">Srivats P</a></p>					
								</th>
							</tr>
							<tr>
								<th>4:45pm - 5:00pm</th>
								<th><h4>Q & A</h4></th>
							</tr>
							<tr>
								<th>5:15pm - 6:15pm</th>
								<th><img src="img/littlefin.png">
								<img src="img/littlefin.png">
									<h4>23: Solving Real World Case Studies</h4>
									<p>Instructor: <a href="bios/kary-rogers">Kary Rogers</a></p>
								</th>
							</tr>
							<tr>
								<th>6:15pm - 6:30pm</th>
								<th><h4>Q & A</h4></th>
							</tr>
						</tbody>
					</table>
					</div>
					<div id="lvl24-tab-second" class="table-responsive container tab-pane fade">
							<table class="schedule-table table">
								<tbody>
									<tr>
										<th>9:30am - 10:30am</th>
										<th><img src="img/littlefin.png">
											<h4>14: USB Analysis 101</h4>
											<p>Instructor: <a href="bios/tomasz-mon">Tomasz Mon</a></p>
										</th>
									</tr>
									<tr>
										<th>10:30am - 10:45am</th>
										<th><h4>Q & A</h4></th>
									</tr>
									<tr>
										<th>11:00am - 12:00pm</th>
										<th><img src="img/littlefin.png">
										<img src="img/littlefin.png">		
											<h4>16: The Packet Doctors are in! Packet trace examinations with the experts</h4>
											<p>Instructors: Drs. <a href="bios/hansang-bae">Bae</a>, <a href="bios/sake-blok">Blok</a>, <a href="bios/jasper-bongertz">Bongertz</a> & <a href="bios/christian-landstrom">Landström</a></p>
										</th>
									</tr> 
									<tr>
										<th>12:00pm - 12:15pm</th>
										<th><h4>Q & A</h4></th>
									</tr>
									<tr>
										<th>12:15pm - 12:45pm</th>
										<th><h4>Lunch</h4></th>
									</tr>
									<tr>
										<th>12:45pm - 1:45pm</th>
										<th><img src="img/littlefin.png">
											<h4>18: Intrusion Analysis and Threat Hunting with Suricata</h4>
											<p>Instructors: <a href="bios/josh-stroschein">Josh Stroschein</a> and <a href="bios/jack-mott">Jack Mott</a></p>
										</th>
									</tr>
									<tr>
										<th>1:45pm - 2:00pm</th>
										<th><h4>Q & A</h4></th>
									</tr>
									<tr>
										<th>2:15pm - 3:15pm</th>
										<th><img src="img/littlefin.png">
											<h4>20: Practical Signature Development for Open Source IDS</h4>
											<p>Instructor: <a href="bios/jason-williams">Jason Williams</a> and <a href="bios/jack-mott">Jack Mott</a></p>
										</th>
									</tr> 
									<tr>
										<th>3:15pm - 3:30pm</th>
										<th><h4>Q & A</h4></th>
									</tr>
									<tr>
										<th>3:45pm - 4:45pm</th>
										<th><img src="img/littlefin.png"><img src="img/littlefin.png">
											<h4>22: Introduction to WAN Optimization</h4>
											<p>Instructor: <a href="bios/erick-powell.php">John Pittle</a></p>					
										</th>
									</tr>
									<tr>
										<th>4:45pm - 5:00pm</th>
										<th><h4>Q & A</h4></th>
									</tr>
									<tr>
										<th>5:15pm - 6:15pm</th>
										<th><img src="img/littlefin.png"><img src="img/littlefin.png">
											<h4>24: Analyzing 802.11 Powersave Mechanisms with Wireshark</h4>
											<p>Instructor: <a href="bios/george-cragg.php">George Cragg</a></p>
										</th>
									</tr>
									<tr>
										<th>6:15pm - 6:30pm</th>
										<th><h4>Q & A</h4></th>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div> 
</div>
</div>

<?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php"); ?>
