<?php include($_SERVER['DOCUMENT_ROOT'] . "/header.php"); ?>

<div class="container-fluid speakers-page">
	<div class="col-sm-3">
		<img src="/img/speakers-large/betty.jpg">
	</div>
	<div class="col-sm-9">
		<h2>Betty DuBois - Chief Detective @ Network Detectives</h2>
		<p>Betty is the Chief Detective for Packet Detectives, and has been solving mysteries since 1997.</p>
		<p>She troubleshoots the root cause of network and/or application issues. Experienced with a range of hardware and software solutions, she captures the right data, in the 

		right place, and at the right time. Using packets to solve crimes against the network and applications, is her passion. Teaching others how to do the same, is her calling.
		</p>
	</div>
</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php"); ?>