<?php include($_SERVER['DOCUMENT_ROOT'] . "/header.php"); ?>

<div class="container-fluid speakers-page">
	<div class="col-sm-3">
		<img src="/img/speakers-large/boyd-stephens.jpg">
	</div>
	<div class="col-sm-9">
		<h2>Boyd Stephens, Founder, Netelysis</h2>
		<p>
		Netelysis founder, Boyd Stephens has spent well over a decade as an entrepreneur within the network design, administration, and analyst space. His capture tools of choice are tshark and tcpdump both of which he finds very advantageous in supporting networks that are located in regions of the country where network throughput/bandwidth is limited and service levels are very susceptible to environmental conditions. His firm is totally dependent upon the "*shark" in all of its activities which range from trace file/network flow/performance management analysis to general network design and deployment and all things in between.</p>
	</div>
</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php"); ?>