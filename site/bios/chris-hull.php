<?php include($_SERVER['DOCUMENT_ROOT'] . "/header.php"); ?>

<div class="container-fluid speakers-page">
	<div class="col-sm-3">
		<img src="/img/speakers-large/chris-hull.jpg">
	</div>
	<div class="col-sm-9">
		<h2>Chris Hull, Distinguished Engineer, Capital One</h2>
		<p>
			Chris is a network engineer and packet analysis expert, currently working network operations at Capital One. Chris has previously worked at OPNET/Riverbed, first as a developer, and later in professional services. In OPNET, he developed and led the STAR24 service, where they provided a quick response, guaranteed, application and network performance troubleshooting service. This experience has carried through to Capital One, where he provides network incident top-level escalation analyses and supports the move to a zero datacenter footprint.
		</p>
	</div>
</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php"); ?>