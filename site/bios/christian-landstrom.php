<?php include($_SERVER['DOCUMENT_ROOT'] . "/header.php"); ?>

<div class="container-fluid speakers-page">
	<div class="col-sm-3">
		<img src="/img/speakers-large/christian-landstrom.jpg">
	</div>
	<div class="col-sm-9">
		<h2>Christian Landström - Senior Consultant @ Airbus Defence and Space</h2>
		<p>
		Christian Landström works as Incident Response and security audit expert at Airbus Defence and Space CyberSecurity. Working in IT from 2004, with a strong focus on network communications and IT security, he graduated in computer science in 2008 and joined Synerity Systems and afterwards moved with the whole Synerity team to work for Fast Lane GmbH. There Christian created and delivered various Network Analysis Trainings and worked as Senior Consultant for network analysis and IT security. In 2013 he started working for Airbus Defence and Space CyberSecurity focusing on IT security, Incident Response and Network Forensics. He shares his passion about network analysis together with Jasper and Eddi from the original Synerity Team on the Sharkfest conferences and on <a rhef="https://blog.packet-foo.com">blog.packet-foo.com.</p>
	</div>
</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php"); ?>