<?php include($_SERVER['DOCUMENT_ROOT'] . "/header.php"); ?>

<div class="container-fluid speakers-page">
	<div class="col-sm-3">
		<img src="/img/speakers-large/erick-powell	.jpg">
	</div>
	<div class="col-sm-9">
		<h2>Erick Powell, ENTS, Texas Instruments</h2>
		<p>
		Erick Powell is a senior network SME at a fortune 200 company.  Areas of expertise include routing/switching, packet broker network designs, overseeing capture appliance deployments, network monitoring / management / automation / mapping and of course, packet capture analysis. With over 25 years of network experience, Erick has developed and participated in multiple open source projects and has worked with numerous vendors in resolving software bugs and product enhancements. Requested involvement in one such company’s Technical Advisory Board continues due to his inherent drive to ensure tool-sets function as designed. Erick also holds exclusive membership on his company’s highly respected technical expert team.</p>
	</div>
</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php"); ?>