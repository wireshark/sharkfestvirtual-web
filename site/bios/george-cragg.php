<?php include($_SERVER['DOCUMENT_ROOT'] . "/header.php"); ?>

<div class="container-fluid speakers-page">
	<div class="col-sm-3">
		<img src="/img/speakers-large/george-cragg.jpg">
	</div>
	<div class="col-sm-9">
		<h2>George Cragg, Network Engineer, Draeger Medical Systems</h2>
		<p>
		*Started career separating gasses and liquids in a lab and then filling tires with nitrogen<br>
		*Other stops along the way include Six Sigma Black Belt, Industrial Control and Automation, and manufacturing support<br>
		*Now I am a full time network engineer in a software team that makes medical devices to work on Hospital IT networks<br>
		*I started looking at network traffic with Ethereal and after reading Steven's book, I knew this is really what I wanted to do</p>
	</div>
</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php"); ?>