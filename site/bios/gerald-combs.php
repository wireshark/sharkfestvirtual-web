<?php include($_SERVER['DOCUMENT_ROOT'] . "/header.php"); ?>

<div class="container-fluid speakers-page">
	<div class="col-sm-3">
		<img src="/img/speakers-large/gerald.jpg">
	</div>
	<div class="col-sm-9">
		<h2>Gerald Combs, Wireshark Project Founder & Director, Open Source Projects, Riverbed</h2>
		<p>
		Gerald Combs is the original developer of Wireshark, the award-winning network protocol analyzer. He started the project in 1998 (under the name Ethereal) while working at an ISP. Since then, many bright and talented people have contributed to the project. Currently, Gerald is the Director of Open Source Projects for Riverbed Technology, which sponsors Wireshark and WinPcap.</p>
	</div>
</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php"); ?>