<?php include($_SERVER['DOCUMENT_ROOT'] . "/header.php"); ?>

<div class="container-fluid speakers-page">
	<div class="col-sm-3">
		<img src="/img/speakers-large/graham-bloice.jpg">
	</div>
	<div class="col-sm-9">
		<h2>Graham Bloice, Software Developer, Trihedral UK Ltd. & Wireshark Core Developer</h2>
		<p>
		Graham is a Software Developer with Trihedral UK Limited where he helps produce their VTSCada HMI\Scada toolkit. Graham is also a Wireshark core developer, mainly concentrating on the Windows build machinery and DNP3 dissectors. He uses Wireshark frequently in his day job when analysing telemetry protocols used in the SCADA world, and inter-machine traffic for the company’s distributed SCADA product.</p>
	</div>
</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php"); ?>