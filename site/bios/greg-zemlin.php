<?php include($_SERVER['DOCUMENT_ROOT'] . "/header.php"); ?>

<div class="container-fluid speakers-page">
	<div class="col-sm-3">
		<img src="/img/speakers-large/greg-zemlin.jpg">
	</div>
	<div class="col-sm-9 col-centered" style="margin-top: 10px;">
		<h2> Greg Zemlin, Product Manager, Garland Technology</h2>
		<p>
		Greg Zemlin is a Product Manager at Garland Technology, a technology partner with Wireshark. Greg is currently working on the research and development of Garland Technology's new products including the virtual test access point. He has over 8 years of experience in the industry and has extensive expertise in product management, hardware design, and hardware/software verification.</p>
	</div>
</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php"); ?>