<?php include($_SERVER['DOCUMENT_ROOT'] . "/header.php"); ?>

<div class="container-fluid speakers-page">
	<div class="col-sm-3">
		<img src="/img/speakers-large/jasper.jpg">
	</div>
	<div class="col-sm-9">
		<h2>Jasper Bongertz - Senior Consultant @ Airbus Defence and Space</h2>
		<p>
		Jasper Bongertz is a Senior Technical Consultant at Airbus Defence and Space CyberSecurity and started working freelance in 1992 while he began studying computer science at the Technical University of Aachen. In 2009, Jasper became a Senior Consultant and Trainer for Fast Lane, where he created a large training portfolio with a special focus on Wireshark.  In 2013, he joined Airbus Defence and Space CyberSecurity, focusing on IT security, Incident Response and Network Forensics.</p>
	</div>
</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php"); ?>