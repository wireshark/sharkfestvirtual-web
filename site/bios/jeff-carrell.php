<?php include($_SERVER['DOCUMENT_ROOT'] . "/header.php"); ?>

<div class="container-fluid speakers-page">
	<div class="col-sm-3">
		<img src="/img/speakers-large/jeff-carrell.jpg">
	</div>
	<div class="col-sm-9">
		<h2>Jeff Carrell - Network Consultant at Network Conversions</h2>
		<p>
		Jeff Carrell is a Network Instructor at Hewlett Packard Enterprise. Jeff facilitates many networking courses offered by HPE, serves as a SME for customized courses for specific customer requirements, and participates in course development. </p>
		<p>Jeff is a frequent industry speaker, technical writer, IPv6 Forum Certified Trainer, and prior to HPE was a network instructor and course developer to major networking manufacturers. He is a technical lead and co-author for the book, Guide to TCP/IP: IPv6 and IPv4, 5th Edition and lead technical editor on Fundamentals of Communications and Networking, Second Edition. Jeff is a long-time user of Wireshark.</p>
	</div>
</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php"); ?>