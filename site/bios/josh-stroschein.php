<?php include($_SERVER['DOCUMENT_ROOT'] . "/header.php"); ?>

<div class="container-fluid speakers-page">
	<div class="col-sm-3">
		<img src="/img/speakers-large/josh-stroschein.jpg">
	</div>
	<div class="col-sm-9">
		<h2>Josh Stroschein, Open Information Security Foundation</h2>
		<p>
		Josh is an experienced malware analyst and reverse engineer who has a passion for sharing his knowledge with others. He
        is the Director of Training for OISF, where he leads all training activity for the foundation and is also responsible for
        academic outreach and developing research initiatives. Josh is an accomplished trainer, providing training in the
        aforementioned subject areas at BlackHat, DerbyCon, Toorcon, Hack-In-The-Box, Suricon and other public and private
        venues. Josh is an Assistant Professor of Cyber Security at Dakota State University where he teaches malware analysis and
        reverse engineering, an author on Pluralsight, and a threat researcher for Bromium.</p>
	</div>
</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php"); ?>