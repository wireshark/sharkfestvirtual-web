<?php include($_SERVER['DOCUMENT_ROOT'] . "/header.php"); ?>

<div class="container-fluid speakers-page">
	<div class="col-sm-3">
		<img src="/img/speakers-large/kevin-burns.jpg">
	</div>
	<div class="col-sm-9">
		<h2>Kevin Burns, Principal Engineer, Data Center Engineering, Comcast</h2>
		<p>
		Kevin Burns is a Principal Engineer in the Data Center Engineering team at Comcast Communications. He has been involved in network and application troubleshooting since 1995. He is the author of TCP/IP Troubleshooting Toolkit and a past Sharkfest presenter. Kevin loves solving difficult problems, developing troubleshooting tools, and sharing his experiences and knowledge with others.</p>
	</div>
</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php"); ?>