<?php include($_SERVER['DOCUMENT_ROOT'] . "/header.php"); ?>

<div class="container-fluid speakers-page">
	<div class="col-sm-3">
		<img src="/img/speakers-large/maher.jpg">
	</div>
	<div class="col-sm-9">
		<h2>Maher Adib - Principal Consultant, Ofisgate Sdn Bhd</h2>
		<p>
		Maher’s first exposure to packet analysis was in 2000 when he downloaded Ethereal (now known as Wireshark) and was instantly fascinated. His love for the open source analyzer has led to a near-daily commitment to using the tool to discover what is really going on with network infrastructures. As Technical Lead for Ofisgate Sdn Bhd in Kuala Lumpur, he has architected Cyber Range, a realistic training platform for red and blue teaming scenarios mimicking real-world scenarios using Wireshark packet analysis capabilities as one of the primary weapons.  Maher is an active member of,and frequent presenter at, local and international IT-related community events such as Durian Conference, Malaysia Open Source Community meetings and, of course, SharkFest!</p>
		
	</div>
</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php"); ?>