<?php include($_SERVER['DOCUMENT_ROOT'] . "/header.php"); ?>

<div class="container-fluid speakers-page">
	<div class="col-sm-3">
		<img src="/img/speakers-large/paul-offord.jpg">
	</div>
	<div class="col-sm-9">
		<h2>Paul Offord, CTO, TribeLab</h2>
		<p>
		Paul Offord has had a 39-year career in the IT industry that includes roles in hardware engineering, software engineering and network management. Prior to founding Advance7, he worked for IBM, National Semiconductor and Hitachi Data Systems. Paul and the Problem Analysts at Advance7 help IT support teams in many business sectors troubleshoot difficult performance and stability problems. Paul has recently contributed code to the Wireshark project and is currently leading the team developing Workbench.</p>
	</div>
</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php"); ?>