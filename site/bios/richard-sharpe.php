<?php include($_SERVER['DOCUMENT_ROOT'] . "/header.php"); ?>

<div class="container-fluid speakers-page">
	<div class="col-sm-3">
		<img src="/img/speakers-large/richard-sharpe.png">
	</div>
	<div class="col-sm-9 col-centered" style="margin-top: 10px;">
		<h2>Richard Sharpe, Principal Software Engineer, Primary Data</h2>
		<p>
		Richard is a long-time contributor to Wireshark who has recently started working on the 802.11 and other dissectors, including theIEEE1905 and other protocols.</p>
	</div>
</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php"); ?>