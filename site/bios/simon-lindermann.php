<?php include($_SERVER['DOCUMENT_ROOT'] . "/header.php"); ?>

<div class="container-fluid speakers-page">
	<div class="col-sm-3">
		<img src="/img/speakers-large/simon.jpg">
	</div>
	<div class="col-sm-9">
		<h2>Simon Lindermann - Network Engineer@Miele</h2>
		<p>
		Since successfully completing his IT Specialist apprenticeship, Simon has been working as a Network Engineer for a German household appliance manufacturer. While working on projects in various locations around the world, he discovered his passion for network analysis, so along with his job at Miele, Simon started doing freelance troubleshooting work, following the slogan “Only packets tell the truth!" </p>
	</div>
</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php"); ?>