<?php include($_SERVER['DOCUMENT_ROOT'] . "/header.php"); ?>

<div class="container-fluid speakers-page">
	<div class="col-sm-3">
		<img src="/img/speakers-large/simone.jpg">
	</div>
	<div class="col-sm-9 col-centered" style="margin-top: 10px;">
		<h2> Simone Mainardi, Senior Data Scientist, ntop</h2>
		<p>
		Simone Mainardi received his BSc, MSc and PhD degrees in Computer Science from the University of Pisa, Faculty of Information Engineering. He worked as a research associate both at the University of Pisa and at the Institute for Informatics and Telematics (IIT) of the Italian National Research Council (CNR). He is now with ntop as a Senior Data Scientist. He is interested in computer networking, parallel and distributed algorithms, Internet measurements and data analysis.</p>
	</div>
</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php"); ?>