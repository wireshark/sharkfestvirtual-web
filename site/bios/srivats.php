<?php include($_SERVER['DOCUMENT_ROOT'] . "/header.php"); ?>

<div class="container-fluid speakers-page">
	<div class="col-sm-3">
		<img src="/img/speakers-large/srivats.jpg">
	</div>
	<div class="col-sm-9 col-centered" style="margin-top: 10px;">
		<h2> Srivats P, Creator of Ostinato, Data Plane Developer at Juniper</h2>
		<p>
		Srivats P is the creator and developer of Ostinato which is a personal project. In his day job, he works as a data plane developer at a leading networking vendor and has developed L2/L3 software for a broad spectrum of devices fom home routers to DSLAMs to edge and core routers over his career spanning more than 20 years.</p>
	</div>
</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php"); ?>