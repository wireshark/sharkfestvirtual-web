<?php include($_SERVER['DOCUMENT_ROOT'] . "/header.php"); ?>

<div class="container-fluid speakers-page">
	<div class="col-sm-3">
		<img src="/img/speakers-large/tomasz-mon.jpg">
	</div>
	<div class="col-sm-9">
		<h2>Tomasz Moń, Software Specialist, Etteplan Poland Sp. z o.o.</h2>
		<p>
		Tomasz is the author of USBPcap - a kernel driver that enables software USB capture on Windows. Tomasz is also a Wireshark Core Developer and contributor to various Open Source projects (e.g. OpenVizsla USB hardware sniffer). Tomasz works as a Software Specialist at the Etteplan office located in Wrocław, Poland. At work, Tomasz develops firmware for embedded systems. He has experience with bare metal systems, RTOS based solutions, Embedded Linux and Windows Drivers.</p>
	</div>
</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php"); ?>