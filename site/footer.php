<footer class="footer">
     <div class="container">
     	<ul class="footer-social">
	        <li><a target="_blank" href="https://www.facebook.com/SHARKFEST-Wireshark-Network-Analysis-Conference-51502216870/"><img src="/img/socialmedia/facebook.jpg"></a></li>
	        <li><a target="_blank" href="https://twitter.com/wiresharkfest"><img src="/img/socialmedia/twitter.jpg"></a></li>
	        <li><a target="_blank" href="https://www.linkedin.com/groups/1802965"><img src="/img/socialmedia/linkedin.jpg"></a></li>
	        <li><a target="_blank" href="https://www.youtube.com/user/SharkFest2015"><img src="/img/socialmedia/youtube.jpg"></a></li>
        </ul>
        <!-- <p class="">
        	<a href="http://www.riverbed.com/us/">© Riverbed Technology</a> — <a href="https://www.riverbed.com/legal/privacy-policy.html">Privacy Policy</a> | <a href="http://www.riverbed.com/us/legal_notices.php">Legal Notices</a> | <a href="mailto: sharkfest@wireshark.org">Contact Us</a> | <a href="mailto: sharkfest@wireshark.org">Registration Assistance</a>
        </p> -->

    </div>
</footer>

<!-- jQuery -->
    <script src="/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/lightbox.min.js"></script>
    <script src="/js/custom.js"></script>
    <script src="/js/countdown.js"></script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-605389-8"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-605389-8');
</script>

</body>

</html>