<?php include($_SERVER['DOCUMENT_ROOT'] . "/header.php"); ?>

<div class="reg-background-image">
</div>
<div class="row grid-row reg-row">
	<div class="col-lg-7 col-centered index-text">
	    <h4>SharkFest'20 Virtual is sold out and the Waiting List is now closed.<br>
			<strong>There are a few seats available for the pre-conference classes.  Register for one of these classes now!</strong><br>
			<strong style="color: red">Everyone registered for the Pre-Conference Classes will automatically receive SharkFest conference registration.<br></strong></h4><br>
	    <div class="">
			<a target="_blank" class="front-button" href="https://cvent.me/Yg5oZ5"><h4>Pre-Conference Registration</h4></a>
		</div>
		<h4>Please note that SharkFest'20 Virtual pre-conference class and sessions times are Pacific Daylight Time.</h4>
	</div>
</div>
<div class="container">
	<div class="row">
		<div class="col-lg-10 col-centered reg-table ">
			<table class="table table-striped">
			  <tbody>
			    <tr class="thead-default">
			      <th scope="row">Registration Fees</th>
			      <!-- <th>Early Bird (Valid through July 31st)</th> -->
			      <a href=""><th>Standard (Valid August 1st)</th></a>
			      <th></th>     
			    </tr>
			    <!--<tr>
			      <th>Early Bird Registration<br>(Until Feb 15th, 2018)</th>
			      <td class="td-centered">$1095</td>
			      <td class="td-centered">$1095</td>
			      <td class="td-centered">$300</td>
			    </tr>-->
			    <tr>
			      <th>SharkFest Conference Only (Oct 15-16)</th>
			      <!-- <td class="td-centered">FREE</td> -->
			      <td class="td-centered">FREE</td>
			      <td class="td-centered"></td>			      
			    </tr>
			    <tr>
			      <th>Pre-Conference Class I: Troubleshooting w/ Wireshark - Core Skills (Oct 12-13)</th>
			      <!-- <td class="td-centered">$745 (€665)</td> -->
			      <td class="td-centered">$795</td>
			      <td class="td-centered"></td> 
			    </tr>
			    <tr>
			      <th>Pre-Conference Class II: Wireshark Profiles - How to Analyze Trace Files Faster/Easier (Oct 14)</th>
			      <!-- <td class="td-centered">$445 (€395)</td> -->
			      <td class="td-centered">$495</td>
			      <td class="td-centered"></td> 
			    </tr>
			    <tr>
			      <th>Pre-Conference Class III: SSL/TLS troubleshooting with Wireshark (Oct 14)</th>
			      <!-- <td class="td-centered">$445 (€395)</td> -->
			      <td class="td-centered">$495</td>
			      <td class="td-centered"></td> 
			    </tr>
				<tr>
			      <th>Pre-Conference Class I + Pre-Conference Class II</th>
			      <!-- <td class="td-centered">$1,190</td> -->
			      <td class="td-centered">$1,290</td>
			      <td class="td-centered"></td> 
			    </tr>
				<tr>
			      <th>Pre-Conference Class I + Pre-Conference Class III</th>
			      <!-- <td class="td-centered">$1,190</td> -->
			      <td class="td-centered">$1,290</td>
			      <td class="td-centered"></td> 
			    </tr>
			  </tbody>
			</table>
		</div>
		<div class="col-lg-10 col-centered">
			<div id="accordion" role="tablist" aria-multiselectable="true" class="reg-accordion">
			  <div class="card">
			    <div class="card-header" role="tab" id="headingTwo">
			      <h5 class="mb-0">
			        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
			           SHARKFEST CONFERENCE DETAILS
			        </a>
			      </h5>
			    </div>
			    <div id="collapseTwo" class="collapse in" role="tabpanel" aria-labelledby="headingTwo">
			      <div class="reg-p">
			        <table class="table table-striped">
					  <tbody>
					    <!--<tr>
					      <th scope="row">What is SharkFest?</th>
					      <th>SharkFest is a highly-interactive educational conference focused on sharing knowledge, experience and best practices among members of the Wireshark global developer and user communities.</th>
					    </tr>
					    <tr>
					      <th scope="row">Why Should I Attend?</th>
					      <th>
					      	
					      		• To benefit from an immersive Wireshark educational experience<br>
					      		• To interact with veteran packet analysts<br>
					      		• To test your Wireshark chops<br>
					      		• To network, socialize, and share knowledge with like-minded Wireshark enthusiasts<br>
					      		• To enhance your Wireshark user experience<br>
					      		• To entertain attendees with your SharkBytes presentation!<br>
					      	
					      </th>
					    </tr> -->				    
					    <tr>
					      <th scope="row">What's the Schedule?</th>
					      <th>
								October 15th: Keynote, Sessions<br>
								October 16th: Keynote, Sessions<br>
						  </th>
					    </tr>
					    <tr>
					      <th scope="row">Location</th>
					      <th>Online</a><br></th>
					    </tr>
					    <tr>
					      <th scope="row">Dates</th>
					      <th>October 12-16, 2020</th>
					    </tr>
					    <!-- <tr>
					      <th scope="row">Where Should I Stay?</th>
					      <th>Click <a href="lodging.php"> HERE</a> to go to the Lodging page.</th>
					    </tr> -->
					    <tr>
					    	<th scope="row">Price</th>
					    	<th>FREE</th>
					    </tr>
					    <!-- <tr>
					      <th scope="row">Payment Methods</th>
					      <th>Payment for SharkFest is by credit card.</th>
					    </tr> -->
					    <!-- <tr>
					      <th scope="row">Available SharkFest Discounts*</th>
					      <th><p>
					      		• Full-time CS students, CS faculty and IT staff of accredited educational institutions<br>
					      		• Active-duty military personnel and veterans<br>
					      		• Returning SharkFesters <br><br>
					      		*Please contact <a href="mailto:sharkfest@wireshark.org"> sharkfest@wireshark.org</a> for discount codes.<br><br>
					      		
					      	  </p>
					     </th> -->
					    </tr>
					  </tbody>
					</table>
					
					<!-- <table class="table table-striped2 cancel-table">
					  <thead class="thead-default">
					    <tr>
					      <th colspan="2">CANCELLATION POLICY</th>
					      <th></th>					      
					    </tr>
					  </thead>
					  <tbody>
					    <tr>
					      <td scope="row">14 days or more before the SharkFest’20 US Conference start date</th>
					      <td>Full Refund minus $100 Administration Fee</td>
					    </tr>
					    <tr>
					      <td scope="row">Less than 14 days before the SharkFest’20 US Conference start date</th>
					      <td>No Refund</td>
					    </tr>
					  </tbody>
					</table>
					<p>All cancellation requests must be made in writing to <a href="mailto:sharkfest@wireshark.org">sharkfest@wireshark.org.</a> If registered but unable to attend, another attendee within your organization may be designated to take your place at no additional charge.  All substitution requests must be submitted by the original attendee via e-mail to <a href="mailto:sharkfest@wireshark.org">sharkfest@wireshark.org.</a> On-site substitutions may be allowed if the substituting attendee provides a written request from the original attendee. -->
			      </div>
			    </div>
			  </div>
			  <div class="card">
			    <div class="card-header" role="tab" id="headingThree">
			      <h5 class="mb-0">
			        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
			           PRE-CONFERENCE CLASS I: TROUBLESHOOTING W/ WIRESHARK - CORE SKILLS CLASS DETAILS AND CANCELLATION POLICY
			        </a>
			      </h5>
			    </div>
			    <div id="collapseThree" class="collapse in" role="tabpanel" aria-labelledby="headingThree">
			      <div class="reg-p">
			        <table class="table table-striped">
					  <tbody>
					  	<tr>
					  		<th scope="row">Course Description</th>
					  		<th>New to Wireshark? Feeling a little rusty? Want to pick up some new skills prior to SharkFest?<br><br>

							In this hands-on pre-conference course, we will cover concepts of the Wireshark Analyzer and core network protocols which will enable attendees to improve their skills in capturing and interpreting network traffic. Rather than capturing large amounts of data and being overwhelmed by the details, attendees will learn how to practically use Wireshark to attack network problems and hone in on the packets that matter.<br><br>

							The concepts in this course will prepare beginners to get the most out of the SharkFest sessions that will follow during the conference, as well as refresh the skills of seasoned analysts. 
							</th>
						</tr>
						<tr>
							<th scope="row">Course Outline<br>Day 1
							</th>
							<th><strong>Troubleshooting with the Wireshark Analyzer</strong><br>
								<ul>
									<li>• How and where to capture packets</li>
									<li>• How to avoid getting overwhelmed </li>
									<li>• TAP vs SPAN vs Direct Capture</li>
									<li>• Setting up Wireshark </li>
									<li>• Configuring Profiles and Coloring Rules</li>
									<li>• Using Custom Columns</li>
									<li>• Filtering for the traffic that matters</li>
									<li>• Visualizing traffic using I/O Graphs</li>
									<li>• Analyzing core network protocols such as ARP, IP, UDP, DNS, DHCP</li>
									<li>• Creating Filter Expressions</li>
								</ul>
							</th>
						</tr>
						<tr>
							<th scope="row">
							Day 2</th>
							<th><strong>Core Protocol Fundamentals using Wireshark TCP and UDP</strong><br>
								<ul>
									<li>• The Handshake</li>
									<li>• TCP Options</li>
									<li>• How TCP Windows work and Window Scaling</li>
									<li>• SACK</li>
									<li>• TCP Congestion Algorithms </li>
									<li>• TCP Stream Graphs</li>
									<li>• Building Filters for Common TCP Issues</li>
									<li>• How Retransmissions work</li>
								</ul>
							</th>
						</tr>
					  	<tr>
					      <th scope="row">Who Should Attend:</th>
					      <th>Network technicians, network engineers, and application developers who are at the beginning stages of packet analysis, or who have some comfort in the field but would like to pick up new tips and protocol training. </th>
					    </tr>
					    <tr>
					      <th scope="row">Dates & Time</th>
					      <th>Oct 12-13, 2020, 8am - 5pm</th>
					    </tr>
					    <tr>
					      <th scope="row">Location</th>
					      <th>Online</th>
					    </tr>
					    <tr>
					      <th scope="row">Instructor</th>
					      <th>Chris Greer</th>
					    </tr>
					    <tr>
					      <th scope="row">Payment Methods</th>
					      <th>Payment is by credit card.</th>
					    </tr>
					  </tbody>
					</table>
					
					<table class="table table-striped2 cancel-table">
					  <thead class="thead-default">
					    <tr>
					      <th colspan="2">CANCELLATION POLICY</th>
					      <th></th>					      
					    </tr>
					  </thead>
					  <tbody>
					    <tr>
					      <td scope="row">14 days or more before the class start date</th>
					      <td>Full Refund minus $50 ($100 with a bundle) administrative fee</td>
					    </tr>
					    <tr>
					      <td scope="row">Less than 14 days before the class start date</th>
					      <td>No Refund</td>
					    </tr>
					  </tbody>
					</table>
					<p>All cancellation requests must be made in writing to <a href="mailto:sharkfest@wireshark.org">sharkfest@wireshark.org.</a> If registered but unable to attend, another attendee within your organization may be designated to take your place at no additional charge.  All substitution requests must be submitted by the original attendee via e-mail to <a href="mailto:sharkfest@wireshark.org">sharkfest@wireshark.org.</a> On-site substitutions may be allowed if the substituting attendee provides a written request from the original attendee.</p>

			      </div>
			    </div>
			    <div class="card">
			    <div class="card-header" role="tab" id="headingFour">
			      <h5 class="mb-0">
			        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
			           PRE-CONFERENCE CLASS II: WIRESHARK PROFILES - HOW TO ANALYZE TRACE FILES FASTER + EASIER CLASS DETAILS AND CANCELLATION POLICY
			        </a>
			      </h5>
			    </div>
			    <div id="collapseFour" class="collapse in" role="tabpanel" aria-labelledby="headingFour">
			      <div class="reg-p">
			        <table class="table table-striped">
					  <tbody>
					    <tr>
					      <th scope="row">Course Description</th>
					      <th>This hands-on pre-conference course will speed up your ability to interpret Wireshark trace files. Profiles should not be a one size fits all solution. Wireshark profiles can be huge timesavers, if you invest the time up front configuring them to be specific to your environment and workflow.<br><br> 


						The course will cover best practices for creating profiles, determining what should be in a master profile, and making changes and additions to settings. Trace files will be distributed so changes can be used to analyze actual data. Students will leave class with multiple new profiles to use back at the office.<br><br>

						 

						The concepts in this course will also prepare attendees to get the most out of the SharkFest sessions that will follow during the conference, enabling them to progressively heighten their skills throughout the week.</th>
					    </tr>
					    <tr>
					      <th scope="row">Course Outline</th>
					      <th><strong>Wireshark Profiles - A methodical approach to creation and use.</strong>
					      	<ul>
					      		<li>• Categories; by location or by protocol</li>
					      		<li>• Saving profiles to different locations </li>
					      		<li>• Copying what you have already created</li>
					      		<li>• Creating a master profile</li>
					      		<li>• Determining which protocol settings to changes</li>
					      		<li>• Configuring Name Resolution</li>
					      		<li>• Creating Expert settings</li>
					      		<li>• Enabling/disabling protocols</li>
					      		<li>• Creating Custom Columns</li>
					      		<li>• Enhancing I/O Graphs</li>
								<li>• Configuring Color Rules</li>
								<li>• Use different profiles to analyze traces</li>
					      	</ul> 						  
					      </th>
					    </tr>
					    </tr>
					    <tr>
					      <th scope="row">Who Should Attend:</th>
					      <th>Network technicians, network engineers, cybersecurity analysts, security engineers and application developers who are at the beginning to intermediate stages of packet analysis. The course will be focused on core concepts around Wireshark, helping attendees become more efficient and confident analyzing trace files. </th>
					    </tr>					    
					    <tr>
					      <th scope="row">Dates & Time</th>
					      <th>Oct 14, 2020, 8am - 5pm</th>
					    </tr>
					    <tr>
					      <th scope="row">Location</th>
					      <th>Online</th>
					    </tr>
					    <tr>
					      <th scope="row">Instructors</th>
					      <th>Betty DuBois</th>					      		
					     </th>
					    </tr>
					    <tr>
					      <th scope="row">Payment Methods</th>
					      <th>Payment is by credit card.</th>
					    </tr>
					  </tbody>
					</table>
					
					<table class="table table-striped2 cancel-table">
					  <thead class="thead-default">
					    <tr>
					      <th colspan="2">CANCELLATION POLICY</th>
					      <th></th>					      
					    </tr>
					  </thead>
					  <tbody>
					    <tr>
					      <td scope="row">14 days or more before the class start date</th>
					      <td>Full Refund minus $50 ($100 with a bundle) administrative fee</td>
					    </tr>
					    <tr>
					      <td scope="row">Less than 14 days before the class start date</th>
					      <td>No Refund</td>
					    </tr>
					  </tbody>
					</table>
					<p>All cancellation requests must be made in writing to <a href="mailto:sharkfest@wireshark.org">sharkfest@wireshark.org.</a> If registered but unable to attend, another attendee within your organization may be designated to take your place at no additional charge.  All substitution requests must be submitted by the original attendee via e-mail to <a href="mailto:sharkfest@wireshark.org">sharkfest@wireshark.org.</a> On-site substitutions may be allowed if the substituting attendee provides a written request from the original attendee.
			      </div>
			    </div>
			  </div>
			  </div>
			  <div class="card">
			    <div class="card-header" role="tab" id="headingFive">
			      <h5 class="mb-0">
			        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
			           PRE-CONFERENCE CLASS III: SSL/TLS TROUBLESHOOTING WITH WIRESHARK CLASS DETAILS AND CANCELLATION POLICY
			        </a>
			      </h5>
			    </div>
			    <div id="collapseFive" class="collapse in" role="tabpanel" aria-labelledby="headingFive">
			      <div class="reg-p">
			        <table class="table table-striped">
					  <tbody>
			    		<th scope="row">Course Description</th>
					    <th>The applications of today depend more and more on secure communication channels. For most internet applications the TLS protocol (still mostly referred to as SSL) is providing the secure channel to communicate over. To be able to troubleshoot problems with Applications that use (mutual) TLS, one must understand how TLS sessions are set up, how certificates and certificate authorities come into play and how you can look inside the encrypted traffic to analyse the (cleartext) application data. In this session you will gain a better understanding of the operation of the TLS protocol and more importantly, you will learn how to troubleshoot TLS based communications when things don't work as expected.</th>
					    <tr>
					      <th scope="row">Course Outline</th>
					      <th>
					      	<ul>
					      		<li><strong>TLS fundamentals</strong></li>
					      		<li>• Why TLS (and what happened to SSL)?</li>
					      		<li>• Cryptology 101 </li>
					      		<li>• Understanding The TLS protocol</li>
					      		<li>• The TLSv1.2 handshake</li>
					      		<li>• The TLSv1.3 handshake</li>
					      		<li>• Troubleshooting TLS handshakes</li>
					      		<li>• LAB exercises</li>
					      	</ul> 	
					      	<ul>
					      		<li><strong>TLS continued</strong></li>
					      		<li>• Understanding mutual TLS (Authentication based on TLS client certificates)</li>
					      		<li>• Understanding TLS session resumption </li>
					      		<li>• Analysing encrypted application data (without decrypting it)</li>
					      		<li>• Setting up a configuration profile for TLS analysis</li>
					      		<li>• LAB exercises</li>
					      	</ul> 
					      	<ul>
					      		<li><strong>Decrypting TLS traffic</strong></li>
					      		<li>• Decryption based on the private key of the server</li>
					      		<li>• When will this work and when won't it work</li>
					      		<li>• Decryption based on the TLS session keys</li>
					      		<li>• When will this work and when won't it work</li>
					      		<li>• how to get TLS session keys</li>
					      		<li>• LAB exercises</li>
					      	</ul> 					  
					      </th>
					    </tr>
					    <tr>
					      <th scope="row">Who Should Attend:</th>
					      <th>Network technicians, network engineers, cybersecurity analysts, security engineers and application developers who are at the beginning to intermediate stages of packet analysis. The course will be focused on core concepts around Wireshark, helping attendees become more efficient and confident analyzing trace files. </th>
					    </tr>					    
					    <tr>
					      <th scope="row">Dates & Time</th>
					      <th>Oct 14, 2020, 8am - 5pm</th>
					    </tr>
					    <tr>
					      <th scope="row">Location</th>
					      <th>Online</th>
					    </tr>
					    <tr>
					      <th scope="row">Instructor</th>
					      <th>Sake Blok</th>					      		
					     </th>
					    </tr>
					    <tr>
					      <th scope="row">Payment Methods</th>
					      <th>Payment is by credit card.</th>
					    </tr>
					  </tbody>
					</table>
					
					<table class="table table-striped2 cancel-table">
					  <thead class="thead-default">
					    <tr>
					      <th colspan="2">CANCELLATION POLICY</th>
					      <th></th>					      
					    </tr>
					  </thead>
					  <tbody>
					    <tr>
					      <td scope="row">14 days or more before the class start date</th>
					      <td>Full Refund minus $50 ($100 with a bundle) administrative fee</td>
					    </tr>
					    <tr>
					      <td scope="row">Less than 14 days before the class start date</th>
					      <td>No Refund</td>
					    </tr>
					  </tbody>
					</table>
					<p>All cancellation requests must be made in writing to <a href="mailto:sharkfest@wireshark.org">sharkfest@wireshark.org.</a> If registered but unable to attend, another attendee within your organization may be designated to take your place at no additional charge.  All substitution requests must be submitted by the original attendee via e-mail to <a href="mailto:sharkfest@wireshark.org">sharkfest@wireshark.org.</a> On-site substitutions may be allowed if the substituting attendee provides a written request from the original attendee.
			      </div>
			    </div>
			  </div>
			</div>
		</div>
	</div>
</div>


<?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php"); ?>