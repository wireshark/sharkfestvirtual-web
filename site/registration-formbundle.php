<?php

if ((isset($_POST['Tel'])) && (strlen(trim($_POST['Tel'])) > 0)) {
    $Tel = stripslashes(strip_tags($_POST['Tel']));
} else {
    $Tel = 'No First Name entered';
}

if ((isset($_POST['lastName'])) && (strlen(trim($_POST['lastName'])) > 0)) {
    $lastName = stripslashes(strip_tags($_POST['lastName']));
} else {
    $lastName = 'No Last Name entered';
}

if ((isset($_POST['Name'])) && (strlen(trim($_POST['Name'])) > 0)) {
    $Name = stripslashes(strip_tags($_POST['Name']));
} else {
    $Name = 'No Organization entered';
}

if ((isset($_POST['Address'])) && (strlen(trim($_POST['Address'])) > 0)) {
    $Address = stripslashes(strip_tags($_POST['Address']));
} else {
    $Address = 'No Address entered';
}

if ((isset($_POST['Address2'])) && (strlen(trim($_POST['Address2'])) > 0)) {
    $Address2 = stripslashes(strip_tags($_POST['Address2']));
} else {
    $Address2 = 'No Address2 entered';
}

if ((isset($_POST['City'])) && (strlen(trim($_POST['City'])) > 0)) {
    $City = stripslashes(strip_tags($_POST['City']));
} else {
    $City = 'No City entered';
}

if ((isset($_POST['State'])) && (strlen(trim($_POST['State'])) > 0)) {
    $State = stripslashes(strip_tags($_POST['State']));
} else {
    $State = 'No State/Province entered';
}

if ((isset($_POST['Country'])) && (strlen(trim($_POST['Country'])) > 0)) {
    $Country = stripslashes(strip_tags($_POST['Country']));
} else {
    $Country = 'No Country entered';
}

if ((isset($_POST['Postal'])) && (strlen(trim($_POST['Postal'])) > 0)) {
    $Postal = stripslashes(strip_tags($_POST['Postal']));
} else {
    $Postal = 'No Postal Code entered';
}

if ((isset($_POST['Phone'])) && (strlen(trim($_POST['Phone'])) > 0)) {
    $Phone = stripslashes(strip_tags($_POST['Phone']));
} else {
    $Phone = 'No Phone # entered';
}

if ((isset($_POST['Cell'])) && (strlen(trim($_POST['Cell'])) > 0)) {
    $Cell = stripslashes(strip_tags($_POST['Cell']));
} else {
    $Cell = 'No Cell # entered';
}

if ((isset($_POST['Email'])) && (strlen(trim($_POST['Email'])) > 0)) {
    $Email = stripslashes(strip_tags($_POST['Email']));
} else {
    $Email = 'No Email entered';
}

$atop3 = $_POST['formtop3'];
  if(empty($atop3))
  {
    echo("You didn't select anything.");
  }
  else
  {
    $N = count($atop3);

    echo("You selected $N option(s): ");
    for($i=0; $i < $N; $i++)
    {
      echo($atop3[$i] . " ");
    }
  }
/*
$aTitle = $_POST['formTitle'];
  if(empty($aTitle))
  {
    echo("You didn't select anything.");
  }
  else
  {
    $N = count($aTitle);

    echo("You selected $N option(s): ");
    for($i=0; $i < $N; $i++)
    {
      echo($aTitle[$i] . " ");
    }
  } */

$Title = $_POST["Title"];


if ((isset($_POST['Other_Title'])) && (strlen(trim($_POST['Other_Title'])) > 0)) {
    $Other_Title = stripslashes(strip_tags($_POST['Other_Title']));
} else {
    $Other_Title = 'No Other entered';
}

$Interests = $_POST['formInterests'];
  if(empty($Interests))
  {
    echo("You didn't select anything.");
  }
  else
  {
    $N = count($Interests);

    echo("You selected $N option(s): ");
    for($i=0; $i < $N; $i++)
    {
      echo($Interests[$i] . " ");
    }
  }

if ((isset($_POST['Other_Interests'])) && (strlen(trim($_POST['Other_Interests'])) > 0)) {
    $Other_Interests = stripslashes(strip_tags($_POST['Other_Interests']));
} else {
    $Other_Interests = 'No Interests entered';
}

$FirstYear = $_POST["FirstYear"];

if ((isset($_POST['HeardAboutSF'])) && (strlen(trim($_POST['HeardAboutSF'])) > 0)) {
    $HeardAboutSF = stripslashes(strip_tags($_POST['HeardAboutSF']));
} else {
    $HeardAboutSF = 'Did not say how they heard about SF';
}

$WelDinner = $_POST["WelDinner"];
$Hotel = $_POST["Hotel"];
$HotelShuttle = $_POST["HotelShuttle"];
$Shirt = $_POST["Shirt"];
$Diet = $_POST["Diet"];

if ((isset($_POST['Other_Diet'])) && (strlen(trim($_POST['Other_Diet'])) > 0)) {
    $Other_Diet = stripslashes(strip_tags($_POST['Other_Diet']));
} else {
    $Other_Diet = 'No Diet Preference entered';
}

$Accom = $_POST["Accom"];

if ((isset($_POST['Other_Accom'])) && (strlen(trim($_POST['Other_Accom'])) > 0)) {
    $Other_Accom = stripslashes(strip_tags($_POST['Other_Accom']));
} else {
    $Other_Accom = 'No Accomodation Preference entered';
}


$aVisa = $_POST['formVisa'];
  if(empty($aVisa))
  {
    echo("You didn't select anything.");
  }
  else
  {
    $N = count($aVisa);

    echo("You selected $N option(s): ");
    for($i=0; $i < $N; $i++)
    {
      echo($aVisa[$i] . " ");
    }
  }

if ((isset($_POST['EmerName'])) && (strlen(trim($_POST['EmerName'])) > 0)) {
    $EmerName = stripslashes(strip_tags($_POST['EmerName']));
} else {
    $EmerName = 'No Emergency Name entered';
}

if ((isset($_POST['EmerPhone'])) && (strlen(trim($_POST['EmerPhone'])) > 0)) {
    $EmerPhone = stripslashes(strip_tags($_POST['EmerPhone']));
} else {
    $EmerPhone = 'No Emergency Phone entered';
}

ob_start();
?>
<html>
<head>
    <style type="text/css">
    </style>
</head>
<body> 
<h1>BUNDLE</h1>
<table bgcolor="#eeeeff" width="550" border="0" cellspacing="0" cellpadding="15">
    
    <tr>
        <td>BUNDLE</td>
    </tr>
    <tr>

        <td>First Name</td>
        <td><?php echo $Tel; ?></td>
        </tr>
        <tr>
        <td>Last Name</td>
        <td><?php echo $lastName; ?></td>
        </tr>
        <tr>
        <td>Organization</td>
        <td><?php echo $Name; ?></td>
        </tr>
        <tr>
        <td>Address</td>
        <td><?php echo $Address; ?></td>
        </tr>
                <tr>
        <td>Address 2</td>
        <td><?php echo $Address2; ?></td>
        </tr>
        <tr>
        <td>City</td>
        <td><?php echo $City; ?></td>
        </tr>
        <tr>
        <td>State/Province</td>
        <td><?php echo $State; ?></td>
        </tr>
        <tr>
        <td>Country</td>
        <td><?php echo $Country; ?></td>
        </tr>
        <tr>
        <td>Postal Code</td>
        <td><?php echo $Postal; ?></td>
        </tr>
        <tr>
        <td>Phone</td>
        <td><?php echo $Phone; ?></td>
        </tr>
        <tr>
        <td>Cell</td>
        <td><?php echo $Cell; ?></td>
        </tr>
        <tr>
        <td>Email</td>
        <td><?php echo $Email; ?></td>
        </tr>
        <tr>
        <td>Top Reasons for Attending SF</td>
        <td><?php echo("option(s): ");
                    for($i=0; $i < $N; $i++)
                    {echo($atop3[$i] . " ");} ?></td>
    </tr>
        <tr>
        <td>Job Title</td>
        <td><?php echo $Title; ?></td>
        </tr>
        <tr>
        <td>Other Title</td>
        <td><?php echo $Other_Title; ?></td>
        </tr>
        <tr>
        <td>Interests</td>
        <td><?php echo("option(s): ");
                    for($i=0; $i < $N; $i++)
                    {echo($Interests[$i] . " ");} ?></td>
        </tr>
        <tr>
        <td>Other Interests</td>
        <td><?php echo $Other_Interets; ?></td>
        </tr>
        <tr>
        <td>First Year Attending?</td>
        <td><?php echo $FirstYear; ?></td>
        </tr>
        <tr>
        <td>How did you hear about SF?</td>
        <td><?php echo $HeardAboutSF; ?></td>
        </tr>
        <tr>
        <td>Coming to Welcome Dinner?</td>
        <td><?php echo $WelDinner; ?></td>
        </tr>
        <tr>
        <td>Staying at SF Hotel?</td>
        <td><?php echo $Hotel; ?></td>
        </tr>
        <tr>
        <td>Riding Shuttle?</td>
        <td><?php echo $HotelShuttle; ?></td>
        </tr>
        <tr>
        <td>Shirt Size</td>
        <td><?php echo $Shirt; ?></td>
        </tr>
        <tr>
        <td>Diet Preference</td>
        <td><?php echo $Diet; ?></td>
        </tr>
        <tr>
        <td>Other Diet Preference</td>
        <td><?php echo $Other_Diet; ?></td>
        </tr>
        <tr>
        <td>Accomodation Preference</td>
        <td><?php echo $Accom; ?></td>
        </tr>
        <tr>
        <td>Other Accomodation Preference</td>
        <td><?php echo $Other_Accom; ?></td>
        </tr>
        <tr>
        <td>Visa</td>
        <td><?php echo("You selected $N option(s): ");
                for($i=0; $i < $N; $i++)
                {echo($aVisa[$i] . " ");} ?></td>
        </tr>
        <tr>
        <td>Emergency Contact Name:</td>
        <td><?php echo $EmerName; ?></td>
        </tr>
        <tr>
        <td>Emergency Contact Phone</td>
        <td><?php echo $EmerPhone; ?></td>
        </tr>
</table>
</body>
</html>
<?php
$body = ob_get_contents();

$to = 'spampinatodesign@gmail.com, janices@wireshark.org, brenda@chappellu.com, sharkfest@wireshark.org';
$toname = 'Janice Spampinato';
$anotheraddress = '';
$anothername = 'SharkFest Registration Team';

require("phpmailer.php");

$mail = new PHPMailer();

$mail->From = $email;
$mail->FromName = $name;
$mail->AddAddress($to, $toname); // Put your email
//$mail->AddAddress($anotheraddress,$anothername); // addresses here

$mail->WordWrap = 50;
$mail->IsHTML(true);

$mail->Subject = "SharkFest '16 BUNDLE Registration Info";
$mail->Body = $body;
$mail->AltBody = $message;

if (!$mail->Send()) {
    $recipient = $to;
    $subject = 'Registration form failed';
    $content = $body;
    mail($recipient, $subject, $content, "From: $name\r\nReply-To: $email\r\nX-Mailer: DT_formmail");
    exit;
}

header('Location: selection.html');
?>
