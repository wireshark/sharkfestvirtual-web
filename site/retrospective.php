<?php include($_SERVER['DOCUMENT_ROOT'] . "/header.php"); ?>
<div class="container grid-container" id="main-container">
    <section id="main-content">
        <div class="col-md-10 col-sm-12 col-centered">
            <div class="retro-container">
                <div class="container col-md-12">
                    <div style="margin-bottom: 18rem;" class="col-sm-12 col-xs-12 column">
                        <div class="col-xs-12 background-blue front-column1">
                            <div class="grid-cell">
                                <h2>SharkFest'20 Virtual Retrospective</h2>
                                <p>SharkFest’20 Virtual, the first fully online SharkFest, was populated by nearly 1,000 Wireshark enthusiasts from around the world and was a great success in educating and inspire the assembled Wireshark community thanks to the core developers, instructors, sponsors, volunteers, staff, and generous knowledge-sharing by attendees.</p>
                            </div>
                            <div class="about-button">
                                <a class="" href="/sf20v"><h4>Read More</h4></a>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php"); ?>
