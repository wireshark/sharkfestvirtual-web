<?php include($_SERVER['DOCUMENT_ROOT'] . "/header.php"); ?>
<div class="container grid-container" id="main-container">
    <!-- Content area -->
    <section id="main-content">
        <div class="content-area retrospective-page post-excerpt">
            <!-- PAGE BLOG -->
            <section class="page-section with-sidebar sidebar-right">
            <div class="container">
            <div class="row">

            <!-- Content -->
            <section id="content" class="content col-sm-7 col-md-8">

                <article class="post-wrap" data-animation="fadeInUp" data-animation-delay="100">
                    <div class="post-header">
                        
                        <div class="post-meta">
      
                        </div>
                    </div>
                    <div class="post-body">
                        <div class="post-excerpt">
                            <h3 class="post-title2"><strong>Keynote Presentations</strong></h3>
                                <div class="retro-vid-wrapper">
                                     <div class="responsive-iframe">
                                        <iframe width="560" height="315" src="https://www.youtube.com/embed/2Q0juwavIzU" frameborder="0" allowfullscreen></iframe>
                                    </div>
                                    <h4><strong>What's New with Wireshark?</a></strong><br/>
                                    Gerald Combs, Sake Blok, and Roland Knall</h4>
                                </div>
                                <div class="retro-vid-wrapper">
                                    <div class="responsive-iframe">
                                        <iframe width="560" height="315" src="https://www.youtube.com/embed/7jxvhpxAOfg" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                    </div>
                                    <h4><strong>A Bit About Zeek and Spicy</strong><br/>
                                    Vern Paxson and Robin Sommer</h4>
                                </div>  
   
                            <h3 class="post-title2"><strong>Thursday Sessions</strong></h3>
                            <ul style="list-style:none;">
                                <li>01: <a href="/assets/presentations20/01.zip">BACNet and Wireshark for Beginners</a> by Werner Fischer</li>
                                <ul>
                                    <li class="presVideo"><a href="https://youtu.be/QxCMxXjqkyA" title="Presentation video on YouTube" target="_blank">Presentation Video</a></li>
                                </ul>

                                <li>02: <a href="/assets/presentations20/02.pdf">Going down the retransmission hole</a> by Sake Blok</li>
                                <ul>
                                    <li class="presVideo"><a href="https://youtu.be/YupQjxPyuUQ" title="Presentation video on YouTube" target="_blank">Presentation Video</a></li>
                                </ul>

                                <li>03: IPv6 security assessment tools (aka IPv6 hacking tools) by Jeff Carrell</li>
                                <ul>
                                    <li class="presVideo"><a href="https://youtu.be/yNoBIpWt-v8" title="Presentation video on YouTube" target="_blank">Presentation Video</a></li>
                                </ul>

                                <li>04: Improving packet capture in the DPDK by Stephen Hemminger</li>
                                <ul>
                                    <li class="presVideo"><a href="https://youtu.be/YhYZ-fGTa_s" title="Presentation video on YouTube" target="_blank">Presentation Video</a></li>
                                </ul>
              
                                <li>05: <a href="/assets/presentations20/05.pptx">Kismet and Wireless Security 101</a> by Mike Kershaw</li>
                                <ul>
                                    <li class="presVideo"><a href="https://youtu.be/z6MzIDwjUmc" title="Presentation video on YouTube" target="_blank">Presentation Video</a></li>
                                </ul>

                                <li>06: Packets! Wait... What? A very improvised last-minute Wireshark talk about things you can find in pcap files that are funny, interesting or weird. I don't know. Let's find out together by Jasper Bongertz</li>
                                <ul>
                                    <li class="presVideo"><a href="https://youtu.be/S7OsIOrvCKw" title="Presentation video on YouTube" target="_blank">Presentation Video</a></li>
                                </ul>

                                <li>07: TLS encryption and decryption: What every IT engineer should know about TLS by Ross Bagurdes</li>
                                <ul>
                                    <li class="presVideo"><a href="https://youtu.be/9cAyDAbMtZY" title="Presentation video on YouTube" target="_blank">Presentation Video</a></li>
                                </ul>
                                
                                <li>08: Why an Enterprise Visibility Platform is critical for effective Packet Analysis? by Keval Shah</li>
                                <ul>
                                    <li class="presVideo"><a href="https://youtu.be/cFGC13xUvKg" title="Presentation video on YouTube" target="_blank">Presentation Video</a></li>
                                </ul>

                                <li>09: <a href="/assets/presentations20/09.zip">Troubleshooting Cloud Network Outages by</a> Chris Hull</li>
                                
                                <li>10: <a href="/assets/presentations20/10.zip">TCP SACK overview & impact on performance (subject to change)</a> by John Pittle</li>
                                <ul>
                                    <li class="presVideo"><a href="https://youtu.be/4wEDcBZb7fU" title="Presentation video on YouTube" target="_blank">Presentation Video</a></li>
                                </ul>

                                <li>11: <a href="/assets/presentations20/11.zip">Automation TIPS & tricks Using Wireshark/tshark in Windows</a> by Megumi Takeshita</li>
                                <ul>
                                    <li class="presVideo"><a href="https://youtu.be/kv97gYCMM5Q" title="Presentation video on YouTube" target="_blank">Presentation Video</a></li>
                                </ul>

                                <li>12: <a href="/assets/presentations20/12.pptx">How Long is a Packet? And Does it Really Matter?</a> by Stephen Donnelly</li>
                                <ul>
                                    <li class="presVideo"><a href="https://www.youtube.com/watch?v=1fU8lvr1Zds" title="Presentation video on YouTube" target="_blank">Presentation Video</a></li>
                                </ul>

                            </ul>

                            <h3 class="post-title2"><strong>Friday Sessions</strong></h3>
                            <ul style="list-style:none;">
                                <li>13: Make the bytes speak to you by Roland Knall</li>
                                <ul>
                                    <li class="presVideo"><a href="https://youtu.be/Sc9kDIidrxA" title="Presentation video on YouTube" target="_blank">Presentation Video</a></li>
                                </ul>

                                <li>14: <a href="/assets/presentations20/14.zip">USB Analysis 101</a> by Tomasz Moń</li> 
                                <ul>
                                    <li class="presVideo"><a href="https://youtu.be/cUljKImph4s" title="Presentation video on YouTube" target="_blank">Presentation Video</a></li>
                                </ul>

                                <li>15: TLS decryption examples by Peter Wu</li>
                                <ul>
                                    <li class="presVideo"><a href="https://youtu.be/v-lDEiA7JPE" title="Presentation video on YouTube" target="_blank">Presentation Video</a></li>
                                </ul>

                                <li>16: The Packet Doctors are in! Packet trace examinations with the experts by Drs. Blok, Greer Landström, Rogers</li> 

                                <li>17: <a href="/assets/presentations20/17.pdf">Analyzing Honeypot Traffic</a> by Tom Peterson</li>
                                <ul>
                                    <li class="presVideo"><a href="https://youtu.be/aQJW3Kx56sY" title="Presentation video on YouTube" target="_blank">Presentation Video</a></li>
                                </ul>
                            
                                <li>18: Intrusion Analysis and Threat Hunting with Suricata by Josh Stroschein and Jack Mott</li>
                                <ul>
                                    <li class="presVideo"><a href="https://youtu.be/0WgyiOxCFzE" title="Presentation video on YouTube" target="_blank">Presentation Video</a></li>
                                </ul>

                                <li>19: The Other Protocols (used in LTE) by Mark Stout</li>
                                <ul>
                                    <li class="presVideo"><a href="https://www.youtube.com/watch?v=6n9jTYLX70s" title="Presentation video on YouTube" target="_blank">Presentation Video</a></li>
                                </ul>

                                <li>20: <a href="/assets/presentations20/20.pdf">Practical Signature Development for Open Source IDS</a> by Jason Williams and Jack Mott</li>
                                <ul>
                                    <li class="presVideo"><a href="https://youtu.be/Y9tem2ryrbU" title="Presentation video on YouTube" target="_blank">Presentation Video</a></li>
                                </ul>

                                <li>21: <a href="/assets/presentations20/21.pdf">Ostinato - craft packets, generate traffic</a> by Srivats P</li>
                                <ul>
                                    <li class="presVideo"><a href="https://youtu.be/1DIs2VIT3bI" title="Presentation video on YouTube" target="_blank">Presentation Video</a></li>
                                </ul>

                                <li>22: Introduction to WAN Optimization by John Pittle</li> 
                                <ul>
                                    <li class="presVideo"><a href="https://youtu.be/IyvlvmdbvZM" title="Presentation video on YouTube" target="_blank">Presentation Video</a></li>
                                </ul>

                                <li>23: Solving Real World Case Studies by Kary Rogers</li>
                                <ul>
                                    <li class="presVideo"><a href="https://youtu.be/Iqk9waColDo" title="Presentation video on YouTube" target="_blank">Presentation Video</a></li>
                                </ul>

                                <li>24: Analyzing 802.11 Powersave Mechanisms with Wireshark by George Cragg</li>
                                <ul>
                                    <li class="presVideo"><a href="https://youtu.be/IFIuCHQBOfE" title="Presentation video on YouTube" target="_blank">Presentation Video</a></li>
                                </ul>
                        </div>
                    </div>
                    
                </article>

            </section>
            <!-- Content -->

            <hr class="page-divider transparent visible-xs"/>

            <!-- Sidebar -->
            <aside id="sidebar" class="sidebar col-sm-5 col-md-4">
                <div class="background-blue front-column1">
                    <div class="grid-cell">
                        <h4 class="widget-title">A Word of Thanks</h4>
                        <p>SharkFest'20 Virtual proved to be a blazing success thanks to the generous, giving community in attendance. Particular thanks to Gerald Combs and his merry band of core developers for inspiring the many first-time participants by opening with a keynote that illuminated the 20+ year history of the project, to instructors who selflessly donated time and wisdom to educate and mentor participants, to sponsors who so generously provided the resources to make the conference possible, and to a staff and volunteer crew who once again went overboard in making the conference as smooth and pleasant an experience as possible for attendees!</p>
                    </div>
                </div>
                    
                <div class="background-blue front-column1">
                    <div class="grid-cell">
                        <div class="sponsor-list">
                            <div class="sponsor-imgs">
                                    <h4>Sponsor Videos</h4>
                                    <div class="retro-vid-wrapper">
                                        <div class="responsive-iframe">
                                            <iframe width="560" height="315" src="https://www.youtube.com/embed/videoseries?list=PLz_ZpPUgiXqMAuchvaE4MH3Uy6X2oAir5" frameborder="0" allowfullscreen></iframe>
                                        </div>
                                    </div>
                                    <h4>Host Sponsor</h4>
                                    <a href="http://www.riverbed.com/" target="_blank"><img class="sponsors" src="img/sponsors/riverbed.png"></a>

                                    <h4>Angel Shark Sponsors</h4>
                                    <a href="https://www.gnet-inc.com/" target="_blank"><img class="sponsors" src="img/sponsors/gnet.png"></a>
                                    <a href="https://www.endace.com/" target="_blank"><img class="sponsors" src="img/sponsors/endace_big.png"></a>
                                    <a href="https://www.gigamon.com/" target="_blank"><img class="sponsors" src="img/sponsors/gigamon-new-big.jpg"></a>
                                    <a href="https://www.fmad.io/" target="_blank"><img class="sponsors" src="img/sponsors/fmadio.jpg"></a>
                                    <a href="https://veelong.com/" target="_blank"><img class="sponsors" src="img/sponsors/veelong.png"></a>
                                
                                    <h4>Honorary Sponsors</h4>
                                    <a href="http://www.lovemytool.com/" target="_blank"><img class="sponsors" src="img/sponsors/lovemtytool_Logo.png"></a>
                            </div>
                                                        
                        </div>
                    </div>
                </div>
            </aside>
            </div>
            </section>
            <!-- /PAGE BLOG -->

        </div>
        <!-- /Content area -->
    </section>
</div>
    <?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php"); ?>



