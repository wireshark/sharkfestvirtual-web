<?php include($_SERVER['DOCUMENT_ROOT'] . "/header.php"); ?>
<div class="text-centered shop-page">
    <section id="main-content">
        <div class="about-container">
            <div id="myShop">
                <a href="https://shop.spreadshirt.com/sharkfest20-virtual">sharkfest20-virtual</a>
            </div>

            <script>
                var spread_shop_config = {
                    shopName: 'sharkfest20-virtual',
                    locale: 'us_US',
                    prefix: 'https://shop.spreadshirt.com',
                    baseId: 'myShop'
                };
            </script>

            <script type="text/javascript"
                    src="https://shop.spreadshirt.com/shopfiles/shopclient/shopclient.nocache.js">
            </script>
        </div>
    </section>
</div>

<?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php"); ?>