<?php include($_SERVER['DOCUMENT_ROOT'] . "/header.php"); ?>

<body class="">
    <div class="header-and-body">
        <!-- Navigation -->
    

    

<div class="container speakapp">
	<div class="col-lg-9 col-centered">
	 				<div class="post-header" style="">
	                    <h2 class="post-title">SharkFest'20 US Speaker Page</h2>
	                    <div class="post-meta">
	                        
	                        
	                    </div>
	                </div>
	                <div class="post-body">
	                    <div class="post-excerpt">

	                        <p>We look forward to welcoming you to <strong>SharkFest'20 US</strong> as a speaker. Please complete the form below to indicate your interest and proposed presentation subject matter.</p>

	                        <h2 class="post-title2"><strong>Submission Guidelines</strong></h2>
	                        <ol id="speakerList">             
	                            <li>Presentations and workshops accepted for this conference are Wireshark-centric, of an educational nature, and include content that is Wireshark-specific.</li>
	                            <li>Speakers are encouraged to submit sessions matching their expertise.</li>
	                            <li>Presentations may be in lecture-only format, lab-based format, interactive audience format, panel format or any format that meets your presentation preferences and fits within an allotted presentation time frame.</li>
	                            <li>Lecture-based presentations are expected to be approximately 75 minutes long (including 15 minutes of Q&A).  Sessions can be extended to cover back-to-back time slots to accommodate the speaker and ensure that the entire content of a presentation is delivered in an unhurried and complete manner.</li>
	                            <li> Lab-based presentations can range from one session length to a full-day time frame as needed.</li>
	                            <li>Please note that duplicate sessions are weeded out.  Should we receive multiple submissions with the same topic, we will contact the duplicate submitter(s) and ask for a revised or alternate topic.</li>              
	                        </ol>

	                        <!--<h2><strong>Requested SharkFest Topics</strong></h2>
	                        <p>We polled SharkFest prior-year attendees for session topic preferences. The results of those polls are below.</p>
	                        <img src="img/survey.png">  -->  

	                        

	                        <h2 class="post-title2"><strong>Your Details</strong></h2>
	<div class="">
	    <form id="form_923064" class="appnitro"  method="post" action="speaker.php">                                    
	        <ul>
	                   <li id="speakerLi"> 
	        <label class="fName" for="fName">First Name<span>*</span> </label>
	        
	            <input id="fName" required name="fName" data-toggle="tooltip" class="speakerForm" type="text" title="First Name is Required" maxlength="55" value=""/> 
	        </li>
	        <li id="speakerLi">
	        <label class="lName" for="lName">Last Name<span>*</span> </label>
	        
	            <input id="lName" required name="lName" data-toggle="tooltip" class="speakerForm" type="text" title="Last Name is Required" maxlength="55" value=""/> 
	        </li>
	        <li id="speakerLi">
	        <label class="email" for="email">Email Address<span>*</span> </label>
	        
	            <input id="email" required name="email" data-toggle="tooltip" class="speakerForm" type="text" title="Email is Required" maxlength="55" value=""/> 
	        </li>
	        <li id="speakerLi">
	        <label class="cell" for="cell">Cell Phone<span>*</span> (Include Country Code if Outside the U.S.) </label>
	        
	            <input id="cell" required name="cell" class="speakerForm" type="text" maxlength="15" value=""/> 
	         
	        </li>       <li id="speakerLi">
	        <label class="description" for="company">Company/Organization Name<span>*</span> </label>
	        
	            <input id="company" required name="company" class="speakerForm" type="text" maxlength="30" value=""/> 
	        
	        </li>       <li id="speakerLi">
	        <label class="title" for="title">Title/Job Role<span>*</span> </label>
	         
	            <input id="title" required name="title" class="speakerForm" type="text" maxlength="30" value=""/> 
	         
	        </li>       <li id="speakerLi">
	        <label class="description" for="address">Street Address<span>*</span> </label>
	         
	            <input id="address" required name="address" class="speakerForm" type="text" maxlength="50" value=""/> 
	         
	        </li>       <li id="speakerLi">
	        <label class="description" for="address2">Street Address 2</label>
	         
	            <input id="address2"  name="address2" class="speakerForm" type="text" maxlength="50" value=""/> 
	          
	        </li>       <li  id="speakerLi" >
	        <label class="description" for="city">City<span>*</span> </label>
	        
	            <input id="city" required name="city" class="speakerForm" type="text" maxlength="30" value=""/> 
	         
	        </li>       <li  id="speakerLi">
	        <label class="description" for="state">State/Province<span>*</span> </label>
	        
	            <input id="state" required name="state" class="speakerForm" type="text" maxlength="20" value=""/> 
	        
	        </li>       <li id="speakerLi">
	        <label class="description" for="zip">Postal Code<span>*</span> </label>
	        
	            <input id="zip" required name="zip" class="speakerForm" type="text" maxlength="10" value=""/> 
	        
	        </li>       <li id="speakerLi">
	        <label class="shirtSize" for="shirt">Shirt Size<span>*</span> </label>
	        <br>
	        <select class="element select medium" id="shirt" required name="shirt"> 
	            <option value="" selected="selected"></option>
	<option value="small" >Small</option>
	<option value="medium" >Medium</option>
	<option value="large" >Large</option>
	<option value="xl" >XL</option>
	<option value="xxl" >XXL</option>

	        </select>
	        
	        </li>       
	            
	                   
	    </ul>
	        
	    
	                        
	            <h2 class="post-title2"><strong>Biography</strong></h2>

	                <p>Please include a short description of yourself, your work history, your interests, and your relation to Wireshark, Open Source development, security, troubleshooting, etc.</p>

	                <li id="speakerLi">
	                    <label class="description" for="bio">Short Biography (1,000 Characters Maximum)<span>*</span> </label> <br>
	         
	                    <textarea id="bio" required name="bio" class="speakerForm" type="text" maxlength="1000" value=""></textarea>
	          
	                </li>
	            <h2 class="post-title2"><strong>Presentation Information</strong></h2>
	            <ul>
	                <li id="speakerLi">
	                    <label class="description" for="preTitle">Title<span>*</span> </label>
	                    <input id="preTitle" required name="preTitle" class="speakerForm" type="text" maxlength="100" value=""/> 
	                </li>
	                <li id="speakerLi">
	                    <label class="description" for="subTitle">Subtitle </label>
	                    <input id="subTitle" name="subTitle" class="speakerForm" type="text" maxlength="100" value=""/> 
	                </li> 
	                 <li id="speakerLi">
	                    <label class="shirtSize" required for="format">Presentation Format<span>*</span> </label>
	                    <br>
	                    <select class="element select medium" id="format" name="format"> 
	                        <option value="" selected="selected"></option>
	                        <option value="presentation">Presentation</option>
	                        <option value="hands-on">Hands-On Lab</option>
	                        <option value="panel">Panel</option>
	                        <option value="interactive">Audience-Interactive Session</option>
	                    </select>  
	                </li>
	            </ul>
	                <li id="speakerLi">
	                    <label class="description" for="abstract">Presentation Abstract (1,000 Characters Maximum)<span>*</span> </label>
	                    <br>
	                    <textarea id="abstract" required name="abstract" class="speakerForm" type="text" maxlength="1000" value=""></textarea>
	                </li>
	            <ul>
	                <li id="speakerLi">
	                    <label class="shirtSize" for="audience">Audience Expertise Level<span>*</span> </label>
	                    <br>
	                    <select required class="element select medium" id="audience" name="audience"> 
	                        <option value="" selected="selected"></option>
	                        <option value="Beginner">Beginner</option>
	                        <option value="Intermediate">Intermediate</option>
	                        <option value="Advanced">Advanced</option>
	                        <option value="Developer">Developer</option>
	                    </select>  
	                </li>
	            </ul>
	            <h2 class="post-title2"><strong>Additional Questions/Requests</strong></h2>
	                <li id="speakerLi">
	                    <textarea id="additional" name="additional" class="speakerForm" type="text" maxlength="1000" value=""></textarea>
	                </li> 
	                       
	                        


	                             <li class="buttons">
	                                <input type="hidden" name="form_id" value="923064" />
	                
	                                <input id="saveForm" class="button_text" type="submit" name="submit" value="Submit" />
	                            </li>
	                        </form> 
	                    </div>
	                </div>
		</div>
	</div>
</div>

<?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php"); ?>