<?php include($_SERVER['DOCUMENT_ROOT'] . "/header.php"); ?>

<div class="text-centered">
    <div class="sponsor-site-message">
        <p>Visit <a href="https://sharkfestsponsors.wireshark.org">https://sharkfestsponsors.wireshark.org</a> to learn more about the SharkFest'20 Virtual sponsors!</p>
    </div>
    <section id="main-content">
        <div class="container">
        	<div class="row sponsors-page about-container">
                <div class="col-sm-8 col-xs-12 background-blue">
                    <h2>Host Sponsor</h2>
                    <div class="col-lg-12 sponsor-margin-top">
                        <a target="_blank" href="https://www.riverbed.com">
                            <img src="img/sponsors/riverbed.png">
                        </a>
                        <p><strong>Riverbed®</strong> is the leader in Application Performance Infrastructure, delivering the most complete platform for Location-Independent Computing.™ Location-Independent Computing turns location and distance into a competitive advantage by allowing IT to have the flexibility to host applications and data in the most optimal locations while ensuring applications perform as expected, data is always available when needed, and performance issues are detected and fixed before end users notice. Riverbed's 24,000+ customers include 97% of the Fortune 100 and 95% of the Forbes Global 100. Learn more at <a href="https://www.riverbed.com">www.riverbed.com</a>.

                        </p>
                    </div>
                    <div class="col-lg-12 sponsor-padding-bottom">
                        
                    </div>
                    <h2 class="lower-sponsor-header" style="">Angel Shark Sponsor</h2>
                    <div class="col-lg-12 clear-both sponsor-margin-top sponsor-padding-bottom">
                            <a target="_blank" href="https://www.endace.com/">
                                <img src="img/sponsors/endace_big.png" />
                            </a>
                            <p><strong>Endace</strong>’s scalable, open packet-capture platform provides network-wide packet recording at speeds of 100Gbps and beyond. With fast, centralized search, powerful data-mining and the ability to integrate with, and host, security and performance monitoring solutions the open EndaceProbe platform puts definitive packet evidence right at your fingertips.</p>
                            <p>Find out why Cisco, Palo Alto Networks, IBM, Splunk, Gigamon, Darktrace and many others partner with Endace to provide 100% accurate Network Recording at <a target="_blank" href="https://www.endace.com">www.endace.com</a> or follow Endace on <a target="_blank" href="https://twitter.com/endace">Twitter</a>, <a target="_blank" href="https://www.linkedin.com/company/endace">LinkedIn</a> and <a target="_blank" href="https://www.youtube.com/user/EndaceVision">Youtube</a>.</p>
                    </div>
                    <div class="col-lg-12 clear-both sponsor-margin-top sponsor-padding-bottom">
                            <a target="_blank" href="https://fmad.io/">
                                <img src="img/sponsors/fmadio.jpg" />
                            </a>
                            <p><strong>fmadio</strong> is a Tokyo based startup focused on 100% loss less sustained 1G 10G 40G 100G packet capture systems. FMAD 100G packet capture system won Interop Tokyo 2016 Best of Show award, with a rapidly growing world wide customer base including Fortune 500, Finance, Telecoms, Defense and Network Security. Our systems are affordably priced, excellent WireShark workflow and customer support directly by the engineers who built the product. Contact us any time to get an evaluation system. <a href="http://fmad.io/">http://fmad.io/</a>.</p>
                    </div>
                    <div class="col-lg-12 clear-both sponsor-margin-top sponsor-padding-bottom">
                            <a target="_blank" href="https://www.gnet-inc.com/">
                                <img src="img/sponsors/gnet.png" />
                            </a>
                            <p><strong>G-Net Solutions</strong> helps solve the challenges of Performance, Visibility and Security faced by organizations as they undertake Digital, Network and Security modernization. G-Net has built relationships with some of the biggest and most progressive technology partners in the industry, working closely with them to understand how and where new technologies can be harnessed to continually optimize the IT environment for its clients. G-Net started 15 years ago when a small network of highly skilled IT experts came together to look for better ways of blending technology to create more efficient workplaces. G-Net is focused on its client’s business outcomes, from its engineers to its sales executives, it is driven by its client’s business challenges. Learn more at <a href="https://www.gnet-inc.com/">https://www.gnet-inc.com</a>.</p>
                    </div>
                    <div class="col-lg-12 clear-both sponsor-margin-top sponsor-padding-bottom">
                            <a target="_blank" href="https://www.gigamon.com/">
                                <img src="img/sponsors/gigamon-new.png" />
                            </a>
                            <p><strong>Gigamon®</strong> is the recognized leader in network visibility solutions, delivering the powerful insights needed to see, secure and empower enterprise network. Our solutions accelerate threat detection and incident response times while empowering customers to maximize their infrastructure performance across physical, virtual and cloud networks. Since 2004 we have cultivated a global customer base which includes leading Service Providers, Government Agencies as well as Enterprise NetOps and SecOps teams from more than 80 percent of the Fortune 100. For the full story on how we can help reduce risk, complexity, and cost to meet your business needs, visit our website.</p>
                    </div>
                    <div class="col-lg-12 clear-both sponsor-margin-top sponsor-padding-bottom">
                            <a target="_blank" href="https://veelong.com/" />
                                <img src="img/sponsors/veelong.png">
                            </a>
                            <p><strong><a href="https://veelong.com/">Veelong</a></strong> develops data analytics software solutions. Our network protocol
                            analyzer transcend™ provides many unique features and seamless linkage to popular Wireshark,
                            intrusion detection system Snort and Suricata. With transcend™, you can extract the values of ANY
                            fields visible in Wireshark, and use them for filtering, sorting, binning, and data flow separation. You can
                            get high-level visibility with dashboard or report view; or you can use table, time-series chart,
                            correlation and map views for deep-dive analysis. You can easily embed your knowledge to create
                            custom metrics and event alerts. You can automate data capture, processing and report generation.</p>
                    </div>
                <!-- <h2 class="lower-sponsor-header" style="">Angel Shark Lite Sponsor</h2> -->
                <h2 class="lower-sponsor-header" style="">Honorary Sponsor</h2>
                    <div class="col-lg-12 clear-both sponsor-margin-top sponsor-padding-bottom">
                        <a target="_blank" href="http://www.lovemytool.com/">
                            <img src="img/sponsors/lovemytool.png" />
                        </a>
                        <p><strong>LoveMyTool</strong> is an Open Internet Community for Network Analysis, Monitoring and Management technology reviews. We review open source solutions to cyber evidence technology and articles from top network and protocol analysts and are sponsored by the finest companies in our industry!</p>
                    </div>
                </div>
                <div class="col-sm-4 col-xs-12 column-about">
                    <div class="col-xs-12 background-blue front-column1 about-cell">
                        <img src="img/sponsorpage.jpg">
                        <div class="grid-cell">
                            <p>View the SharkFest’20 Virtual Sponsorship Form to read about the different sponsorship levels available. If your company would like to sponsor SharkFest, please contact <a href="mailto:sharkfest@wireshark.org">sharkfest@wireshark.org</a>.</p>
                        </div>
                        <div class="about-button">
                            <a class="" href="assets/sponsor-doc20.pdf"><h4>Download Sponsorship Form</h4></a>
                        </div>
                    </div>
                    <div class="retro-vid-wrapper background-blue col-xs-12 about-cell">
                        <div class="grid-cell">
                          <div class="sponsor-list">
                            <div class="sponsor-imgs">
                                <h4>Sponsor Videos</h4>
                                <div class="responsive-iframe">
                                    <iframe width="560" height="315" src="https://www.youtube.com/embed/videoseries?list=PLz_ZpPUgiXqMAuchvaE4MH3Uy6X2oAir5" frameborder="0" allowfullscreen></iframe>
                                </div>
                            </div>
                          </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>


<?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php"); ?>